﻿using HWHub.Monitoring;
using HWHub.Player;
using System.Collections;
using UnityEngine;
using UnityEngine.Networking;
using Valve.VR;

using Random = System.Random;

namespace HWHub
{
    public class GameManager : MonoBehaviour
    {
        [SerializeField] public HWPlayer _player => HWPlayer.instance;

        [SerializeField]
        public SessionManager _sessionManager
            => SessionManager.instance;

        [SerializeField] private Random _random;

        public bool IsLaserActivatedByOtherObject => _laserPointedActivated;
        private bool _laserPointedActivated = false;

        public Random GameRandom
        {
            get
            {
                if (_random == null)
                {
                    // If random has not been initialized, we initialize one with a system datetime with it's hashcode as string
                    _random = new Random(System.DateTime.Now.ToString().GetHashCode());
                }
                return _random;
            }
            set => _random = value;
        }

        public SteamVR_Action_Vibration Haptic => _player.Haptic;

        public static GameManager instance = null; // Singleton instance

        public const string USERID_SAVE = "UserID";

        // UserID is displayed in game UI (table in main hub)
        public string UserID => _userID;

        private string _userID = "";

        private void Awake()
        {
            if (instance == null)
            {
                instance = this;
                CollectUserID();
            }
            else if (instance != this)
            {
                Destroy(gameObject);
            }
            DontDestroyOnLoad(this); // We want the singleton object to persist through scenes
        }

        public void CollectUserID()
        {
            if (ES3.KeyExists(USERID_SAVE))
            {
                _userID = ES3.LoadString(USERID_SAVE, "UserID loading...");
                if (_userID == "UserID loading..." || _userID == "")
                {
                    Debug.LogWarning("Something went wrong loading user ID, we try create a new user");
                    StartCoroutine(CreateNewUser());
                }
            }
            else
            {
                // We need to call the backend to create a user since it does not exist
                StartCoroutine(CreateNewUser());
            }
        }

        private IEnumerator CreateNewUser()
        {
            UnityWebRequest req = new UnityWebRequest(Constants.BACKEND_URL + Constants.Paths.GenerateNewUser);
            req.method = "GET";
            req.downloadHandler = new DownloadHandlerBuffer();

            yield return req.SendWebRequest();

            if (req.isNetworkError)
            {
                Debug.Log("Error while sending session: " + req.error);
                _userID = "UserID load error";
            }
            else
            {
                Debug.Log("Received when uploading session: " + req.downloadHandler.text);

                _userID = req.downloadHandler.text;

                ES3.Save(USERID_SAVE, _userID);
            }
        }

        public void ActivateLaserPointer()
        {
            _laserPointedActivated = true;
            _player.ActivateLaserPointer();
        }

        public void DeActivateLaserPointer()
        {
            _laserPointedActivated = false;
            _player.DeActivateLaserPointer();
        }

        // ======================= Other methods =======================
        // In some minigames, using these methods will work.
        // However, they are not used as they might interfere with other components such as menu systems.
        [ContextMenu("Pause")]
        public void Pause()
        {
            Time.timeScale = 0;
        }

        [ContextMenu("Resume")]
        public void Resume()
        {
            Time.timeScale = 1;
        }
    }
}