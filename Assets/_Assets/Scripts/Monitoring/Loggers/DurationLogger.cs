﻿using UnityEngine;

namespace HWHub.Monitoring
{
    public class DurationLogger : MetricLogger
    {
        private float totalDuration = 0;
        private float lastTime;

        protected override void OnLoggingStarted()
        {
            totalDuration = 0;
            lastTime = Time.realtimeSinceStartup;
        }

        protected override void OnLoggingStopped()
        {
        }

        private void FixedUpdate()
        {
            if (active)
            {
                float newTime = Time.realtimeSinceStartup;

                totalDuration += newTime - lastTime;

                lastTime = newTime;

                currentMetric.value = (int)totalDuration;
            }
        }
    }
}