﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace HWHub.Player
{
    public class NotificationPlayer : MonoBehaviour
    {
        public AudioSource audioSource;

        public static NotificationPlayer instance = null; // Singleton instance

        private void Awake()
        {
            if (instance == null) // Singleton instance
            {
                instance = this;
            }
        }

        void Start()
        {
            if (audioSource == null)
                audioSource = GetComponent<AudioSource>();
        }

        
    }
}