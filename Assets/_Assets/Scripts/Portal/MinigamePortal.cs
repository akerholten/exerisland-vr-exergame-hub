﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using Valve.VR;

namespace HWHub.MinigamePortal
{
    public class MinigamePortal : MonoBehaviour
    {
        [SerializeField] private string sceneName;

        private bool collided;

        private void Start()
        {
            this.collided = false;
        }

        private void OnTriggerEnter(Collider other)
        {
            if (!this.collided)
            {
                Debug.Log("Did not collide with body collider: " + other.gameObject.name);
                if (other.gameObject.name.Contains("Collider") && sceneName != "")
                {
                    Debug.Log("Loading Scene");
                    SteamVR_LoadLevel.Begin(this.sceneName, showGrid: true, fadeOutTime: 0.75f);
                    //SceneManager.LoadScene(this.sceneName, LoadSceneMode.Single);

                    this.collided = true;
                }
            }
        }
    }
}