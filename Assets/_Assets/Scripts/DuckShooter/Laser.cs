﻿using System.Collections;
using System;
using UnityEngine;
using Valve.VR;
using Valve.VR.InteractionSystem;
using System.Collections.Generic;

namespace HWHub.DuckShooter
{
    public class Laser : MonoBehaviour
    {
        [SerializeField] private GameObject laser;
        private LineRenderer lineRenderer;
        private DuckShooterManager manager;
        private AudioSource audioSource;

        [Range(0, 50)]
        [SerializeField] private float maxRayLength = 50.0f;

        [SerializeField] private LayerMask TargetLayerMask;

        public SteamVR_Action_Boolean Trigger = SteamVR_Input.GetAction<SteamVR_Action_Boolean>("GrabPinch");

        public enum Events
        {
            TARGET_HIT = 0,
        }

        private bool active = false;

        private bool readyToShoot = false;

        private float laserAliveTime = 0.1f;

        private float laserCooldown = 0.3f;

        private bool previousTriggerState = false;

        // Start is called before the first frame update
        private void Start()
        {
            if (this.lineRenderer == null)
            {
                this.lineRenderer = laser.GetComponent<LineRenderer>();
            }

            if (manager == null)
            {
                manager = FindObjectOfType<DuckShooterManager>();
            }

            if (audioSource == null)
            {
                audioSource = GetComponent<AudioSource>();
            }

            this.lineRenderer = Instantiate(this.lineRenderer);

            this.lineRenderer.gameObject.SetActive(this.active);

            //StartCoroutine(this.DEBUG_SHOOT_LASER());
        }

        private IEnumerator DEBUG_SHOOT_LASER()
        {
            while (true)
            {
                yield return new WaitForSeconds(2.0f);
                this.active = !this.active;
            }
        }

        // Update is called once per frame
        private void Update()
        {
            this.HandleTrigger();
            if (this.lineRenderer == null) return;

            this.lineRenderer.gameObject.SetActive(this.active);
            this.lineRenderer.SetPosition(0, transform.position);
            this.lineRenderer.SetPosition(1, transform.position + transform.forward * maxRayLength);

            if (this.active)
            {
                this.RayCast();
            }
        }

        private void HandleTrigger()
        {
            bool currentTriggerState = Trigger.GetStateDown(SteamVR_Input_Sources.Any);

            // Same trigger state as last time so user has not done anything
            if (previousTriggerState == currentTriggerState)
            {
                return;
            }

            previousTriggerState = currentTriggerState;

            if (!readyToShoot)
            {
                return;
            }

            if (currentTriggerState == true)
            {
                previousTriggerState = true;
                StartCoroutine(ShootGun());
            }
        }

        private void OnDisable()
        {
            active = false;
            readyToShoot = false;

            // Disabling line renderer also
            if (this.lineRenderer != null)
            {
                this.lineRenderer.gameObject.SetActive(false);
            }
        }

        private void OnEnable()
        {
            // Hacky way to show when game started and then enable this
            StartCoroutine(GameStarted());
        }

        private IEnumerator ShootGun()
        {
            this.active = true;
            readyToShoot = false;

            PlayRandomSoundFromList(manager.shootClips, manager.shootClipVolume);
            yield return new WaitForSeconds(laserAliveTime);
            this.active = false;

            yield return new WaitForSeconds(laserCooldown - laserAliveTime);
            readyToShoot = true;
        }

        public IEnumerator GameStarted()
        {
            yield return new WaitForSeconds(0.2f);
            active = false;
            readyToShoot = true;
        }

        private void PlayRandomSoundFromList(List<AudioClip> clips, float volume)
        {
            if (clips == null || clips.Count == 0)
            {
                return;
            }

            int soundToPlayId = GameManager.instance.GameRandom.Next() % clips.Count;

            PlaySoundOnce(clips[soundToPlayId], volume);
        }

        private void PlaySoundOnce(AudioClip clip, float volume)
        {
            if (audioSource != null)
            {
                audioSource.PlayOneShot(clip, volume);
            }
        }

        private void RayCast()
        {
            RaycastHit hitInfo = new RaycastHit();

            Target targetHit = null;

            if (Physics.Raycast(transform.position, transform.forward, out hitInfo, maxRayLength, TargetLayerMask))
            {
                this.lineRenderer.SetPosition(1, hitInfo.point); // The visual laser only reaches where it hit something

                targetHit = hitInfo.collider.GetComponent<Target>();

                if (targetHit != null)
                {
                    manager.HandleLaserHit(Events.TARGET_HIT, targetHit);
                }
                else { Debug.Log("EROROROROROROROR " + hitInfo.collider.gameObject.name); }
            }
        }
    }
}