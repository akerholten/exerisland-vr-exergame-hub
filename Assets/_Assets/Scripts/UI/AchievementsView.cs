﻿using HWHub.Metrics;
using HWHub.Player;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace HWHub.UI
{
    public class AchievementsView : MonoBehaviour
    {
        public GameObject achievementUI_Prefab;

        private void OnEnable()
        {
            TearDownUI();
            BuildUI();
        }

        private void OnDisable()
        {
            //TearDownUI();
        }

        private void BuildUI()
        {
            // Add completed achievements first, then from most progress -> least

            // Some quick functionality for now: TODO: Fix for proper sorting
            foreach (Achievement achievement in AchievementManager.instance.Achievements)
            {
                GameObject newVisualAchievement = Instantiate(achievementUI_Prefab, transform);

                newVisualAchievement.GetComponent<AchievementUI>().BuildUI(achievement);
            }
        }

        private void TearDownUI()
        {
            foreach (Transform child in transform)
            {
                Destroy(child.gameObject);
            }
        }
    }
}