﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace HWHub.DuckShooter
{
    public class Target : MonoBehaviour
    {
        private const string LOG_PREFIX = "[DUCK_SHOOTER_TARGET]";

        public float movementSpeed = 2.0f;
        public float projectileSpeed = 4.0f;

        private Vector3 nextPos;

        [SerializeField] private AudioSource audioSource;

        private GameObject player;
        private Projectile projectile;

        private DuckShooterManager manager;

        private IEnumerator routine;

        public enum Events
        {
            PROJECTILE_HIT_PLAYER = 0,
        }

        private void Start()
        {
            if (audioSource == null)
            {
                audioSource = GetComponent<AudioSource>();
            }
        }

        //public static event Action<Events> TargetEvent;

        public void StartMoving(Vector3 offset, float radius, float scale)
        {
            routine = Move(offset, radius, scale);
            StartCoroutine(routine);
        }

        private void OnDestroy()
        {
            if (routine != null)
            {
                StopCoroutine(routine);
            }
        }

        public IEnumerator Move(Vector3 offset, float radius, float scale)
        {
            this.nextPos = this.transform.position;

            while (true)
            {
                yield return null;

                // Calculate distance to move
                float step = movementSpeed * Time.deltaTime;

                // Always look at player
                if (this.player != null)
                {
                    this.transform.LookAt(this.player.transform);
                }

                if (Vector3.Distance(this.transform.position, this.nextPos) > 0.001f)
                {
                    this.transform.position = Vector3.MoveTowards(this.transform.position, this.nextPos, step);
                }
                else
                {
                    this.nextPos = offset + (UnityEngine.Random.insideUnitSphere * radius * scale);
                    StartCoroutine(this.Attack());
                }
            }
        }

        private IEnumerator Attack()
        {
            if (this.projectile == null) yield break;

            Projectile clone = Instantiate(this.projectile, transform.position, transform.rotation);
            if (manager == null)
            {
                manager = FindObjectOfType<DuckShooterManager>();
            }
            PlayRandomSoundFromList(manager.enabledClips, manager.enabledClipVolume);
            clone.SetVelocity(Vector3.Normalize(this.player.transform.position - clone.transform.position) * this.projectileSpeed);
            clone.HitEvent += this.HandleProjectileHits;

            //Destroy the object after 10 sec
            Destroy(clone.gameObject, 10.0f);

            yield return new WaitForSeconds(5.0f);
            clone.HitEvent -= this.HandleProjectileHits;
        }

        private void PlayRandomSoundFromList(List<AudioClip> clips, float volume)
        {
            if (clips == null || clips.Count == 0)
            {
                return;
            }

            int soundToPlayId = GameManager.instance.GameRandom.Next() % clips.Count;

            PlaySoundOnce(clips[soundToPlayId], volume);
        }

        private void PlaySoundOnce(AudioClip clip, float volume)
        {
            if (audioSource != null)
            {
                audioSource.PlayOneShot(clip, volume);
            }
        }

        private void HandleProjectileHits(Projectile.Events eventType, Projectile projectile)
        {
            switch (eventType)
            {
                case Projectile.Events.PLAYER_HIT:
                    if (manager == null)
                    {
                        manager = FindObjectOfType<DuckShooterManager>();
                    }
                    manager.HandleTargetHit(Events.PROJECTILE_HIT_PLAYER);

                    Destroy(projectile.gameObject);
                    break;

                case Projectile.Events.OTHER_HIT:
                    //GameObject.Destroy(projectile.gameObject);
                    break;

                default:
                    break;
            }
        }

        public void Destroy()
        {
            Destroy(this.gameObject);
        }

        public void SetPlayer(GameObject player)
        {
            this.player = player;
        }

        public void SetProjectile(Projectile projectile)
        {
            this.projectile = projectile;
        }

        public void SetManager(DuckShooterManager m)
        {
            this.manager = m;
        }
    }
}