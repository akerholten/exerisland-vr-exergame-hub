﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace HWHub.Metrics
{
    public class AchievementManager : MonoBehaviour
    {
        public class Progress
        {
            public string id;
            public int value;

            public Progress()
            {
            }

            public Progress(string id, int value)
            {
                this.id = id;
                this.value = value;
            }
        }

        public const string SAVE_KEY = "achievementProgress";

        public List<Achievement> availableAchievements;

        public List<Achievement> Achievements => myAchievements;
        private List<Achievement> myAchievements;

        private List<Progress> progressData;

        [SerializeField] private AudioClip achievementUnlockedSound;

        public static AchievementManager instance = null; // Singleton instance

        private void Awake()
        {
            if (instance == null)
            {
                instance = this;
                LoadAchievementProgress();
            }
            else if (instance != this)
            {
                Destroy(gameObject);
            }
            DontDestroyOnLoad(this); // We want the singleton object to persist through scenes
        }

        private void LoadAchievementProgress()
        {
            // to avoid doing changes to the ScriptableObject we copy like this?

            myAchievements = new List<Achievement>();
            foreach (var ach in availableAchievements)
            {
                myAchievements.Add(Instantiate(ach));
            }

            // Load loads the achievement progress from list or creates a list of achievments from the default list
            progressData = ES3.Load(SAVE_KEY, new List<Progress>());

            // Handle if there are achievements that exist in availableAchievements that does not exist in the myAchievements list
            UpdateMyAchievements(false);
        }

        // UpdateMyAchievements updates the actual lists of achievements after new achievementsProgress data has been added
        private void UpdateMyAchievements(bool displayUnlocks = false)
        {
            foreach (var progress in progressData)
            {
                List<Achievement> achievementsToUpdate = myAchievements.FindAll(x => x.progressID == progress.id && !x.Completed);

                foreach (var achievement in achievementsToUpdate)
                {
                    //Updates to either the value or goal, determined by who is the smallest
                    achievement.progress = Mathf.Min(progress.value, achievement.goal);

                    // If display unlocks are enabled and achievement was just completed, we display this info to user
                    if (displayUnlocks && achievement.progress == achievement.goal)
                    {
                        // Play achievement unlocked sound and display UI showing the achievement unlocked
                        UI.MessagePopup.instance.DisplayAchievementUnlocked(achievement);

                        Player.NotificationPlayer.instance.audioSource.PlayOneShot(achievementUnlockedSound, 0.7f);
                    }
                }
            }
        }

        // UpdateProgressData updates list of progress data
        public void UpdateProgressData(string id, int value)
        {
            Progress progressToUpdate = progressData.Find(x => x.id == id);

            if (progressToUpdate == null)
            {
                progressToUpdate = new Progress(id, value);
                progressData.Add(progressToUpdate);
            }
            else
            {
                progressToUpdate.value += value;
            }

            ES3.Save(SAVE_KEY, progressData);

            // Update the list of achievments and display if a new achievements has been unlocked
            UpdateMyAchievements(true);
        }

        [ContextMenu("DEBUG SOUND")]
        private void DebugNotificationSound()
        {
            Player.NotificationPlayer.instance.audioSource.PlayOneShot(achievementUnlockedSound, 0.8f);
        }
    }
}