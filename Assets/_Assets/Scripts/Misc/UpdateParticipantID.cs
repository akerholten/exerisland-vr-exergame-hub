﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace HWHub
{
    public class UpdateParticipantID : MonoBehaviour
    {
        private Text text;
        private bool textUpdated = false;

        // Start is called before the first frame update
        private void Start()
        {
            text = GetComponent<Text>();
            if (text == null)
            {
                Debug.Log("Something went wrong, because Text was null");
            }
            if (GameManager.instance == null || GameManager.instance.UserID.Length < 2)
            {
                StartCoroutine(CheckForUserID(3.0f));
            }
            else
            {
                text.text = GameManager.instance.UserID;
                textUpdated = true;
            }
        }

        private IEnumerator CheckForUserID(float seconds)
        {
            while (!textUpdated)
            {
                if (GameManager.instance != null && GameManager.instance.UserID.Length > 2)
                {
                    text.text = GameManager.instance.UserID;
                    textUpdated = true;
                }
                yield return new WaitForSeconds(seconds);
            }
        }
    }
}