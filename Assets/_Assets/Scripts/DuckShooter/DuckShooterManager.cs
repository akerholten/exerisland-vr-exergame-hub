﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using HWHub.PlatformGame;

namespace HWHub.DuckShooter
{
    public class DuckShooterManager : MiniGameManager
    {
        private const string LOG_PREFIX = "[DUCK_SHOOTER_MANAGER]";

        [System.Serializable]
        public struct Level
        {
            public int targetLimit;
            public float spawnDelay;
            public int totalTargets;
            public Target target;
            public Projectile projectile;
        }

        [SerializeField] private GameObject targetArea;
        [SerializeField] private GameObject gunObject;

        [SerializeField] private Text currentLevelText;
        [SerializeField] private Text livesText;
        [SerializeField] private Text scoreText;

        [SerializeField] private int scoreIncrement = 100;
        [SerializeField] private List<Level> levels;

        private GameObject player;
        private int currentLevelIndex;

        private List<Target> activeTargets;

        private int spawnedTargets; // Total targets spawned per level

        // Easy = 75% gamespeed, Medium = 150% gamespeed, Hard = 225% gamespeed, Extreme = 300% gamespeed
        private float spawnTimeDivisionPerDifficulty = 0.75f;

        private int initialLives = 5;
        private int lives;

        //private IEnumerator spawnRoutine;

        public List<AudioClip> shootClips;

        [Range(0.0f, 1.0f)]
        public float shootClipVolume = 0.3f;

        #region Loggers

        [SerializeField] private Monitoring.ValueLogger scoreLogger;
        [SerializeField] private Monitoring.ValueLogger hitLogger;
        [SerializeField] private Monitoring.FloatLogger caloriesBurnedLogger;

        protected override void VerifyLoggers()
        {
            if (scoreLogger == null || scoreLogger.metricId != "Score")
            {
                foreach (var logger in FindObjectsOfType<Monitoring.ValueLogger>())
                {
                    if (logger.metricId == "Score")
                    {
                        scoreLogger = logger;
                        break;
                    }
                }

                if (scoreLogger == null)
                {
                    Debug.LogError("scoreLogger was null and was not found in scene!");
                }
            }

            if (hitLogger == null || hitLogger.metricId != "Drones_Hit")
            {
                foreach (var logger in FindObjectsOfType<Monitoring.ValueLogger>())
                {
                    if (logger.metricId == "Drones_Hit")
                    {
                        hitLogger = logger;
                        break;
                    }
                }

                if (hitLogger == null)
                {
                    Debug.LogError("hitLogger was null and was not found in scene!");
                }
            }

            if (caloriesBurnedLogger == null || caloriesBurnedLogger.metricId != "Calories_Burned")
            {
                foreach (var logger in FindObjectsOfType<Monitoring.FloatLogger>())
                {
                    if (logger.metricId == "Calories_Burned")
                    {
                        caloriesBurnedLogger = logger;
                        break;
                    }
                }

                if (caloriesBurnedLogger == null)
                {
                    Debug.LogError("caloriesBurnedLogger was null and was not found in scene!");
                }
            }
        }

        #endregion Loggers

        // About 10% less than the other games
        protected override int MinCaloriesBurnedPerHour => 200;

        protected override int CalorieDifficultyLevelTop => 4; // Update if difficulty will be implemented

        public override string MinigameID => "DroneShooter_Minigame";

        public override string GameExplanationText => "Shoot drones with right-hand trigger, avoid missiles.";

        // Start is called before the first frame update
        private void Start()
        {
            this.player = GameObject.Find("HeadCollider");
            if (this.player == null)
            {
                Debug.Log(LOG_PREFIX + " No player object was set");
                return;
            }

            if (this.livesText == null || this.scoreText == null || this.currentLevelText == null)
            {
                Debug.Log(LOG_PREFIX + " missing text elements in scene");
            }

            if (gameMenu == null)
                gameMenu = FindObjectOfType<PlatformGameMenu>();

            if (musicSource == null)
            {
                musicSource = GetComponent<AudioSource>();
            }

            if (gunObject == null)
            {
                Debug.LogWarning("GUN Object reference not set");
            }

            VerifyLoggers();

            // Show menu instead
            gameMenu.gameObject.SetActive(true); // Boot game menu
        }

        private IEnumerator LogCalories(float seconds)
        {
            while (_playing)
            {
                yield return new WaitForSeconds(seconds);
                caloriesBurnedLogger.AddTempValue(GetCaloriesBurned(seconds));
            }
        }

        private void OnDestroy()
        {
            //Target.TargetEvent -= this.HandleTargetEvents;
            //Laser.LaserEvent -= this.HandleLaserEvents;
        }

        public override void StartGame()
        {
            base.StartGame();
            gunObject.SetActive(true);
            currentLevelIndex = 0;
            spawnedTargets = 0;
            lives = initialLives;

            activeTargets = new List<Target>();

            UpdateLivesText();
            UpdateScoreText();
            UpdateCurrentLevelText();

            //Target.TargetEvent -= this.HandleTargetEvents;
            //Target.TargetEvent += this.HandleTargetEvents;
            //Laser.LaserEvent -= this.HandleLaserEvents;
            //Laser.LaserEvent += this.HandleLaserEvents;

            StartPlayingRandomSong();
            StartCoroutine(LogCalories(3.0f));
            StartCoroutine(SpawnInitialTargets());
            //StartCoroutine(DEBUG_DELETE_RANDOM_TARGETS());
        }

        private void HandlePlayerHit()
        {
            this.lives--;

            PlayRandomSoundFromList(errorClips, errorClipVolume);

            this.UpdateLivesText();

            if (this.lives == 0)
            {
                GameOver();
            }
        }

        public override void GameOver()
        {
            foreach (var target in this.activeTargets)
            {
                if (target != null)
                {
                    Destroy(target.gameObject);
                }
            }

            activeTargets = new List<Target>();

            //if (spawnRoutine != null)
            //{
            //    StopCoroutine(spawnRoutine);
            //}

            // Hack to solve issue with spawning continuing after game is over
            StopAllCoroutines();
            //yield return new WaitForSeconds(2.0f);
            //this.livesText.text = "Restarting";
            //yield return new WaitForSeconds(2.0f);

            // Update leaderboards

            // Reload entire scene
            //Target.TargetEvent -= this.HandleTargetEvents;
            //Laser.LaserEvent -= this.HandleLaserEvents;

            scoreLogger.SetValue(score);

            gunObject.SetActive(false);
            base.GameOver();
        }

        private void UpdateLivesText()
        {
            if (this.lives <= 0)
            {
                this.livesText.text = "Game Over!";
                return;
            }

            this.livesText.text = "";
            for (int i = 0; i < this.lives; i++)
            {
                this.livesText.text += "❤";
            }
        }

        private void UpdateScoreText()
        {
            scoreText.text = score.ToString();
        }

        private void UpdateCurrentLevelText()
        {
            this.currentLevelText.text = "Level: " + (this.currentLevelIndex + 1).ToString();
        }

        private IEnumerator SpawnInitialTargets()
        {
            Level lvl = this.levels[this.currentLevelIndex];

            float spawnRadius = this.targetArea.GetComponent<SphereCollider>().radius;
            Vector3 lScale = this.targetArea.transform.localScale;
            float avgScale = (lScale.x + lScale.y + lScale.z) / 3;

            while (this.spawnedTargets < lvl.targetLimit)
            {
                this.SpawnTarget(lvl, spawnRadius, avgScale);
                yield return new WaitForSeconds(lvl.spawnDelay / (spawnTimeDivisionPerDifficulty * difficultyLevel));
            }
        }

        private void SpawnTarget(Level lvl, float spawnRadius, float avgScale)
        {
            Target target = Instantiate(lvl.target, this.RandomTargetPos(), Quaternion.identity);

            target.SetManager(this);
            target.SetPlayer(this.player);
            target.SetProjectile(lvl.projectile);

            target.StartMoving(this.targetArea.transform.position, spawnRadius, avgScale);

            this.activeTargets.Add(target);
            this.spawnedTargets++;
        }

        private IEnumerator HandleTargetHit(Target target)
        {
            Destroy(target.gameObject);

            this.score += this.scoreIncrement * (this.currentLevelIndex + 1);
            this.UpdateScoreText();

            Level lvl = this.levels[this.currentLevelIndex];
            this.activeTargets.Remove(target);

            // Wait for 2 seconds, then spawn new target
            yield return new WaitForSeconds(lvl.spawnDelay / (spawnTimeDivisionPerDifficulty * difficultyLevel));

            if (this.spawnedTargets < lvl.totalTargets)
            {
                float spawnRadius = this.targetArea.GetComponent<SphereCollider>().radius;
                Vector3 lScale = this.targetArea.transform.localScale;
                float avgScale = (lScale.x + lScale.y + lScale.z) / 3;

                this.SpawnTarget(lvl, spawnRadius, avgScale);
            }
            else if (this.activeTargets.Count == 0 && this.spawnedTargets == lvl.totalTargets)
            {
                this.HandleLevelCompleted();
            }
        }

        private void HandleLevelCompleted()
        {
            this.spawnedTargets = 0;

            if (currentLevelIndex < this.levels.Count - 1)
            {
                this.currentLevelIndex++;
                this.UpdateCurrentLevelText();
                StartCoroutine(SpawnInitialTargets());

                return;
            }

            // We keep going with the same difficulty we just had
            this.UpdateCurrentLevelText();
            StartCoroutine(SpawnInitialTargets());
        }

        // Update is called once per frame
        private void Update()
        {
        }

        public void HandleLaserHit(Laser.Events eventType, Target targetHit)
        {
            switch (eventType)
            {
                case Laser.Events.TARGET_HIT:
                    Metrics.AchievementManager.instance.UpdateProgressData("Drones_Hit", 1);
                    hitLogger.Add(1);
                    StartCoroutine(this.HandleTargetHit(targetHit));
                    break;

                default:
                    break;
            }
        }

        public void HandleTargetHit(Target.Events eventType)
        {
            switch (eventType)
            {
                case Target.Events.PROJECTILE_HIT_PLAYER:
                    this.HandlePlayerHit();
                    break;

                default:
                    break;
            }
        }

        private Vector3 RandomTargetPos()
        {
            float spawnRadius = this.targetArea.GetComponent<SphereCollider>().radius;

            Vector3 lScale = this.targetArea.transform.localScale;
            float avgScale = (lScale.x + lScale.y + lScale.z) / 3;

            return this.targetArea.transform.position + (Random.insideUnitSphere * spawnRadius * avgScale);
        }
    }
}