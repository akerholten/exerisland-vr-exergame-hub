﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace HWHub.PlatformGame
{
    /// <summary>
    /// Class that deals with containing PlatformObstacles and PlatformHittables
    /// and keeping them in e.g. a 3x3 grid.
    /// </summary>
    public class PlatformMovingBlock : MonoBehaviour
    {
        [SerializeField] private Vector2Int gridSize;
        [SerializeField] private BoxCollider boxCollider;
        [SerializeField] private float secondsToMiddlePos = 1.0f;

        public float SecondsToEndPosition => secondsToEndPos;
        [SerializeField] private float secondsToEndPos = 5.0f;

        public float SecondsMoved => secondsMoved;
        private float secondsMoved = 0.0f;
        private float lastTime = 0.0f;

        public List<Transform> childNodes;

        public IEnumerator MoveAlongPath(PlatformGameManager manager)
        {
            lastTime = Time.realtimeSinceStartup;
            while (transform.position != manager.blockEndPos.position) // While not at the end position yet
            {
                yield return new WaitForFixedUpdate();
                secondsMoved += (Time.realtimeSinceStartup - lastTime) * manager.gameSpeed;
                lastTime = Time.realtimeSinceStartup;

                if (secondsMoved <= secondsToMiddlePos) // If still moving to middle block
                {
                    transform.position = Vector3.Lerp(manager.blockStartPos.position, manager.blockMiddlePos.position, secondsMoved / secondsToMiddlePos);
                }
                else // If moving to endblock/player
                {
                    if (secondsMoved >= secondsToMiddlePos + secondsToEndPos)
                        secondsMoved = secondsToMiddlePos + secondsToEndPos;
                    transform.position = Vector3.Lerp(manager.blockMiddlePos.position, manager.blockEndPos.position, (secondsMoved - secondsToMiddlePos) / secondsToEndPos);
                }
            }
            // When end position is reached, destroy this object and handle whatever data needs to be handled
            Destroy(gameObject, 0.5f); // destroy after 0.5 seconds
        }

        [ContextMenu("Update Grid Nodes")]
        private void UpdateGridNodes()
        {
            var childList = transform.Cast<Transform>().ToList();
            foreach (var child in childList)
            {
                DestroyImmediate(child.gameObject);
            }

            Bounds bounds = boxCollider.bounds;

            // Multiplied with 2 because extents is already halfExtents
            float x_offset = (bounds.extents.x / gridSize.x) * 2.0f;
            float y_offset = (bounds.extents.y / gridSize.y) * 2.0f;

            childNodes = new List<Transform>();
            for (int y = 0; y < gridSize.y; y++)
            {
                for (int x = 0; x < gridSize.x; x++)
                {
                    Vector3 newPos = new Vector3(x * x_offset, y * y_offset, bounds.center.z);
                    GameObject newObj = new GameObject("childNode" + x.ToString() + " " + y.ToString());
                    newObj.transform.parent = transform;
                    newObj.transform.position = newPos;

                    childNodes.Add(newObj.transform);
                }
            }
        }

        public void AddObjects(System.Random rand, List<GameObject> hittables, List<GameObject> obstacles, int difficultyLevel)
        {
            // Minimum 1, max 2 at easy difficulty
            // Minimum 1, max 2 at medium difficulty
            // Minimum 1, max 3 at hard difficulty
            // Minimum 1, max 3 at extreme difficulty
            int objectsToAdd = 1 + (rand.Next() % ((2 + (difficultyLevel / 3))));

            Debug.Log("Added objects count: " + objectsToAdd);

            for (int i = 0; i < objectsToAdd; i++)
            {
                GameObject objectToSpawn;
                if (rand.Next() % 2 == 0) // 50/50 to spawn hittable or obstacle
                {
                    objectToSpawn = hittables[rand.Next() % hittables.Count];
                }
                else
                {
                    objectToSpawn = obstacles[rand.Next() % obstacles.Count];
                }

                // Find all nodes that have no child objects
                List<Transform> availableNodes = childNodes.FindAll(x => x.childCount == 0);

                // Instantiate object with one of these available parent nodes
                Instantiate(objectToSpawn, availableNodes[rand.Next() % availableNodes.Count]);
            }
        }
    }
}