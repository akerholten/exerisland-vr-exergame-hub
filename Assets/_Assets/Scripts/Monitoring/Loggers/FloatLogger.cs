﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace HWHub.Monitoring
{
    public class FloatLogger : ValueLogger
    {
        private float tempMetricValue;

        public void SetTempValue(float newValue)
        {
            tempMetricValue = newValue;
        }

        public void AddTempValue(float value)
        {
            tempMetricValue += value;
        }

        protected override void OnLoggingStarted()
        {
            base.OnLoggingStarted();

            tempMetricValue = 0;
        }

        protected override void OnLoggingStopped()
        {
            currentMetric.value = (int)tempMetricValue;
        }
    }
}