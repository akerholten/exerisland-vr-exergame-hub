﻿using UnityEngine;
using Valve.VR;
using Valve.VR.InteractionSystem;

namespace HWHub.Player
{
    public class HWPlayer : MonoBehaviour
    {
        [SerializeField] private Hand _leftHand;

        public Hand LeftHand
        {
            get
            {
                if (_leftHand == null) _leftHand = transform.Find("LeftHand").GetComponent<Hand>();
                return _leftHand;
            }
            set => _leftHand = value;
        }

        [SerializeField] private Hand _rightHand;

        public Hand RightHand
        {
            get
            {
                if (_rightHand == null) _rightHand = transform.Find("LeftHand").GetComponent<Hand>();
                return _rightHand;
            }
            set => _rightHand = value;
        }

        [SerializeField] private UI.MainMenu mainMenu;
        [SerializeField] private UI.LaserPointer laserPointer;

        public SteamVR_Action_Boolean MenuButton = SteamVR_Input.GetAction<SteamVR_Action_Boolean>("MenuButton");
        public SteamVR_Action_Vibration Haptic = SteamVR_Input.GetVibrationAction("Haptic");

        public static HWPlayer instance = null; // Singleton instance

        private void Start()
        {
            if (mainMenu == null)
            {
                Debug.LogWarning("Main menu was null");
                mainMenu = FindObjectOfType<UI.MainMenu>();
            }

            if (laserPointer == null)
            {
                Debug.LogWarning("Laser pointer was null");
                laserPointer = FindObjectOfType<UI.LaserPointer>();
            }
        }

        private void Awake()
        {
            if (instance == null) // Singleton instance
            {
                instance = this;
            }
            /*else if (instance != this)
            {
                Destroy(gameObject);
            }
            DontDestroyOnLoad(this); // We want the singleton object to persist through scenes*/
        }

        private void Update()
        {
            HandleButtonEvents();
        }

        private void HandleButtonEvents()
        {
            if (MenuButton.GetStateDown(SteamVR_Input_Sources.Any)) // Toggle menu
            {
                if (!mainMenu.MenuIsActive)
                {
                    mainMenu.TriggerMainMenuEvent(UI.MainMenu.Events.OPEN_MENU);
                }
                else
                {
                    mainMenu.TriggerMainMenuEvent(UI.MainMenu.Events.EXIT_MENU);
                }
            }
        }

        public void ActivateLaserPointer()
        {
            if (laserPointer == null)
            {
                Debug.LogWarning("Laser pointer was null");
                laserPointer = FindObjectOfType<UI.LaserPointer>();
            }
            laserPointer.Activate();
        }

        public void DeActivateLaserPointer()
        {
            if (laserPointer == null)
            {
                Debug.LogWarning("Laser pointer was null");
                laserPointer = FindObjectOfType<UI.LaserPointer>();
            }
            laserPointer.DeActivate();
        }
    }
}