﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace HWHub.Metrics
{
    public abstract class Metric : MonoBehaviour
    {
        public int value;
        public string metricName;
        public static uint UniqueID = 0;


        public class MetricEventArgs<T> : EventArgs
        {
            public T value;
            public MetricEventArgs(T value)
            {
                this.value = value;
            }
        }

        public delegate void ValueChangedHandler(object sender, MetricEventArgs<int> e);
        public static ValueChangedHandler ValueChanged;

        protected abstract void OnValueChanged();

    }
    /*public abstract class Metric<T> : MonoBehaviour
    {
        public T value
        {
            get { return value; }
            set { this.value = value; OnValueChanged(); }
        }
        public string metricName;
        public static uint UniqueID = 0;


        public class MetricEventArgs : EventArgs
        {
            public T value;
            public MetricEventArgs(T value)
            {
                this.value = value;
            }
        }

        public delegate void ValueChangedHandler(object sender, MetricEventArgs e);
        public static ValueChangedHandler ValueChanged;

        protected virtual void OnValueChanged() { ValueChanged(this, new MetricEventArgs(value)); }

    }*/
}
