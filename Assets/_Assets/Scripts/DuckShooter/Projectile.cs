﻿using System;
using UnityEngine;

namespace HWHub.DuckShooter
{

public class Projectile : MonoBehaviour
{

    public enum Events
    {
        TARGET_HIT = 0,
        PLAYER_HIT = 1,
        OTHER_HIT = 2,
    }

    public event Action<Events, Projectile> HitEvent;

    void OnTriggerEnter(Collider other)
    {
        if (HitEvent == null)
            return;

        string collideObjectName = other.gameObject.name;

        if (collideObjectName == "TargetNavigationArea") return;

        if (this.IsPlayer(collideObjectName))
        {
            HitEvent(Events.PLAYER_HIT, this);
        }
        else if (this.IsTarget(collideObjectName))
        {
            HitEvent(Events.TARGET_HIT, this);
        }
        else
        {
            HitEvent(Events.OTHER_HIT, this);
        }
    }

    public void SetVelocity(Vector3 velocity)
    {
        Rigidbody body = this.GetComponent<Rigidbody>();
        body.velocity = velocity;
    }

    private bool IsPlayer(string name)
    {
        if (name == "HeadCollider")
            return true;

        return false;
    }

    private bool IsTarget(string name)
    {
        return name.Contains("Drone");
    }
}
}
