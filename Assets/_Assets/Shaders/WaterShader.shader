﻿// Inspired from: https://roystan.net/articles/toon-water.html

Shader "HWHub/WaterShader"
{
    Properties
    {
		//_MainColor("_MainColor", Color) = (1.0, 1.0, 1.0)
        //_MainTex("Texture", 2D) = "white" {}
		_DepthGradientShallow("Depth Gradient Shallow", Color) = (0.325, 0.807, 0.971, 0.725)
		_DepthGradientDeep("Depth Gradient Deep", Color) = (0.086, 0.407, 1, 0.749)
		_DepthMaxDistance("Depth Maximum Distance", Float) = 1
		_SurfaceNoise("Surface Noise", 2D) = "white" {}
		_SurfaceNoiseCutoff("Surface Noise Cutoff", Range(0, 1)) = 0.777
		_SurfaceNoiseScroll("Surface Noise Scroll Amount", Vector) = (0.03, 0.03, 0, 0)
		_FoamDistance("Foam Distance", Float) = 0.4

		// Two channel distortion texture.
		_SurfaceDistortion("Surface Distortion", 2D) = "white" {}
		// Control to multiply the strength of the distortion.
		_SurfaceDistortionAmount("Surface Distortion Amount", Range(0, 1)) = 0.27
    }
    SubShader
    {
		
        Tags { "Queue" = "Transparent" "RenderType"="Opaque" }
        LOD 100

        Pass
        {
			Blend SrcAlpha OneMinusSrcAlpha
			ZWrite Off
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                //float2 uv : TEXCOORD0;
                UNITY_FOG_COORDS(1)
                float4 vertex : SV_POSITION;
				float4 screenPosition : TEXCOORD2;
				float2 noiseUV : TEXCOORD0;
				float2 distortUV : TEXCOORD1;
            };

            //sampler2D _MainTex;
            //float4 _MainTex_ST;
			sampler2D _SurfaceNoise;
			float4 _SurfaceNoise_ST;
			float _SurfaceNoiseCutoff;
			float2 _SurfaceNoiseScroll;
			float _FoamDistance;

			sampler2D _SurfaceDistortion;
			float4 _SurfaceDistortion_ST;

			float _SurfaceDistortionAmount;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                //o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				o.screenPosition = ComputeScreenPos(o.vertex);
				o.noiseUV = TRANSFORM_TEX(v.uv, _SurfaceNoise);
				o.distortUV = TRANSFORM_TEX(v.uv, _SurfaceDistortion);
                UNITY_TRANSFER_FOG(o,o.vertex);
                return o;
            }

			// ----- //
			float4 _DepthGradientShallow;
			float4 _DepthGradientDeep;

			float _DepthMaxDistance;

			sampler2D _CameraDepthTexture;

			

            fixed4 frag (v2f i) : SV_Target
            {
				float existingDepth01 = tex2Dproj(_CameraDepthTexture, UNITY_PROJ_COORD(i.screenPosition)).r;
				float existingDepthLinear = LinearEyeDepth(existingDepth01);
				float depthDifference = existingDepthLinear - i.screenPosition.w;
				
				// Lerp color/alpha on distance from bottom of the sea and perspective
				float waterDepthDifference01 = saturate(depthDifference / _DepthMaxDistance);
				float4 waterColor = lerp(_DepthGradientShallow, _DepthGradientDeep, waterDepthDifference01);
				
				// Waves over time
				float2 distortSample = (tex2D(_SurfaceDistortion, i.distortUV).xy * 2 - 1) * _SurfaceDistortionAmount;
				float2 noiseUV = float2((i.noiseUV.x + _Time.y * _SurfaceNoiseScroll.x) + distortSample.x, (i.noiseUV.y + _Time.y * _SurfaceNoiseScroll.y) + distortSample.y);;
				float surfaceNoiseSample = tex2D(_SurfaceNoise, noiseUV).r;
				
				// Shoreline foam
				float foamDepthDifference01 = saturate(depthDifference / _FoamDistance);
				float surfaceNoiseCutoff = foamDepthDifference01 * _SurfaceNoiseCutoff;

				// Cutoff value for the waves
				float surfaceNoise = surfaceNoiseSample > surfaceNoiseCutoff ? surfaceNoiseSample : 0;

				// Alternative way to do surface noise, this looks abit more like clouds maybe? Or better sea, idk
				//float surfaceNoise = surfaceNoiseSample > surfaceNoiseCutoff ? lerp(0, 1, (surfaceNoiseSample - surfaceNoiseCutoff) / (1 - surfaceNoiseCutoff)) : 0; // surfaceNoiseSample : 0;

				return waterColor + surfaceNoise;
				// sample the texture
                //fixed4 col = tex2D(_MainTex, i.uv);
                // apply fog
                //UNITY_APPLY_FOG(i.fogCoord, col);
                //return col;
            }
            ENDCG
        }
    }
}
