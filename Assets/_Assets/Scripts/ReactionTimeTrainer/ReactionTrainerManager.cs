﻿using HWHub.PlatformGame;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Valve.VR;

namespace HWHub.ReactionTimeTrainer
{
    // TODO: create scaling difficulty that can be adjusted by player
    // TODO: create main menu that player can navigate pre-play and after play
    // TODO: implement logging loop to backend completely
    public class ReactionTrainerManager : MiniGameManager
    {
        [Header("Reaction Trainer Game Manager Info")]

        // Constants
        private const string LOG_PREFIX = "[GAME_MANAGER]";

        private const string TIMER_OBJECT_NAME = "Timer";
        private const string SCORE_OBJECT_NAME = "Score";
        private const string BUTTON_PANEL_OBJECT_NAME = "ButtonPanel";

        [SerializeField] private List<ReactionTrainerButton> buttons;

        //[SerializeField] private PlatformGameMenu gameMenu;

        //#region Audio

        //// AudioClips and songs

        //public List<AudioClip> impactClips;
        //public List<AudioClip> enabledClips;
        //public List<AudioClip> successClips;
        //public List<AudioClip> errorClips;
        //public List<AudioClip> gameSongs;
        //public List<AudioClip> gameOverSounds;

        //[Range(0.0f, 1.0f)]
        //public float impactClipVolume = 0.5f;

        //[Range(0.0f, 1.0f)]
        //public float enabledClipVolume = 0.5f;

        //[Range(0.0f, 1.0f)]
        //public float successClipVolume = 0.5f;

        //[Range(0.0f, 1.0f)]
        //public float errorClipVolume = 0.5f;

        //[Range(0.0f, 1.0f)]
        //public float gameSongVolume = 0.5f;

        //[Range(0.0f, 1.0f)]
        //public float gameOverVolume = 0.5f;

        //#endregion Audio

        [SerializeField] private Timer timer;

        [SerializeField] private Text scoreText;

        #region Game settings

        // Game settings
        [Header("Game Settings")]
        [SerializeField] private bool godMode = false;

        private float defaultSpawnInterval = 4.0f;

        private float defaultSpawnDuration = 3.2f;

        private int randomIntervalSteps = 100;

        private float interval;
        private float duration;

        private float GetRandomInterval()
        {
            return Mathf.Max(intervalFloor, interval + (intervalDecreasePerDifficultyLevel * 0.5f) - ((intervalDecreasePerDifficultyLevel / randomIntervalSteps) * GameManager.instance.GameRandom.Next(randomIntervalSteps)));
        }

        private float intervalFloor = 0.1f;
        private float durationFloor = 0.2f;

        private float intervalDecreasePerDifficultyLevel = 0.4f;
        private float durationDecreasePerDifficultyLevel = 0.15f;

        private float maxDifficultyLevel = 15.0f;
        private float difficultyIncreasePerMinute = 1.0f;

        private int scoreModifier = 5; // modifies by multiplying with difficulty level

        private int initialLives = 5;

        #endregion Game settings

        private IEnumerator gameRoutine; // Wont be used if pause by timeScale

        // Game states

        private bool paused = false; // Wont be used if pause by timeScale
        private int currentLives;

        private int spawnId;

        public override string MinigameID => "ReactionTimeTrainer_Minigame";
        public override string GameExplanationText => "Hit water bubbles as soon as they activate by turning green" + System.Environment.NewLine + "If you are too slow or miss, you lose 1 life." + System.Environment.NewLine + "Good luck!";

        protected override int CalorieDifficultyLevelTop => 10;

        // Estimate from table-tennis "light" at https://www.kalkuler.com/kalkulator/trening/treningskalkulator/
        protected override int MinCaloriesBurnedPerHour => 222;

        #region Loggers

        [SerializeField] private Monitoring.ValueLogger scoreLogger;
        [SerializeField] private Monitoring.ValueLogger hitLogger;
        [SerializeField] private Monitoring.ReactionTimeLogger reactionTimeLogger;
        [SerializeField] private Monitoring.FloatLogger caloriesBurnedLogger;

        protected override void VerifyLoggers()
        {
            // TODO: rewrite this into using a function VerifyLogger(ValueLogger valueLogger, string metricID) or something
            // TODO: generally rewrite this piece of garbage......... really inefficient and redundant way of doing this..
            if (scoreLogger == null || scoreLogger.metricId != "Score")
            {
                foreach (var logger in FindObjectsOfType<Monitoring.ValueLogger>())
                {
                    if (logger.metricId == "Score")
                    {
                        scoreLogger = logger;
                        break;
                    }
                }

                if (scoreLogger == null)
                {
                    Debug.LogError("scoreLogger was null and was not found in scene!");
                }
            }

            if (hitLogger == null || hitLogger.metricId != "WaterBubbles_Hit")
            {
                foreach (var logger in FindObjectsOfType<Monitoring.ValueLogger>())
                {
                    if (logger.metricId == "WaterBubbles_Hit")
                    {
                        hitLogger = logger;
                        break;
                    }
                }

                if (hitLogger == null)
                {
                    Debug.LogError("hitLogger was null and was not found in scene!");
                }
            }

            if (reactionTimeLogger == null || reactionTimeLogger.metricId != "Average_ReactionTime")
            {
                foreach (var logger in FindObjectsOfType<Monitoring.ReactionTimeLogger>())
                {
                    if (logger.metricId == "Average_ReactionTime")
                    {
                        reactionTimeLogger = logger;
                        break;
                    }
                }

                if (reactionTimeLogger == null)
                {
                    Debug.LogError("averageReactionTimeLogger was null and was not found in scene!");
                }
            }

            if (caloriesBurnedLogger == null || caloriesBurnedLogger.metricId != "Calories_Burned")
            {
                foreach (var logger in FindObjectsOfType<Monitoring.FloatLogger>())
                {
                    if (logger.metricId == "Calories_Burned")
                    {
                        caloriesBurnedLogger = logger;
                        break;
                    }
                }

                if (caloriesBurnedLogger == null)
                {
                    Debug.LogError("caloriesBurnedLogger was null and was not found in scene!");
                }
            }
        }

        #endregion Loggers

        private void Awake()
        {
            if (gameMenu == null)
                gameMenu = FindObjectOfType<PlatformGameMenu>();

            if (musicSource == null)
            {
                musicSource = GetComponent<AudioSource>();
            }

            VerifyLoggers();

            // Disable button event handling until game starts
            DisableButtonCollisions();

            gameMenu.gameObject.SetActive(true); // Boot game menu
        }

        // startGame initializes game states and starts the game
        public override void StartGame()
        {
            base.StartGame();

            if (!this.InitSceneGameObjects())
            {
                Debug.LogError(LOG_PREFIX + " an error occured initializing the scene");
                return;
            }

            // Initialize private members
            //score = 0;
            //gameStartTime = Time.realtimeSinceStartup;
            //gameHasBeenPlayed = true;
            //_playing = true;
            spawnId = 0;
            UpdateLiveDifficulty();
            AdjustGameDifficulty();
            scoreText.text = score.ToString("D3");
            currentLives = initialLives; // Enables changing init lives from editor
            paused = false;

            // save runGame coroutine reference
            this.gameRoutine = this.RunGame();

            // Buttons and their collisions will be disabled until the timer ends
            DisableButtonCollisions();

            // when timer ends, the game will start
            StartCoroutine(this.timer.CountDown());

            //Monitoring.SessionManager.instance.StartNewActivity(MinigameID);
        }

        private void UpdateLiveDifficulty()
        {
            liveDifficultyLevel = (difficultyLevel * 2.0f) + ((difficultyIncreasePerMinute / 60) * GameDuration);

            if (liveDifficultyLevel > maxDifficultyLevel)
            {
                liveDifficultyLevel = maxDifficultyLevel;
            }
        }

        private void AdjustGameDifficulty()
        {
            interval = defaultSpawnInterval - (intervalDecreasePerDifficultyLevel * liveDifficultyLevel);
            duration = defaultSpawnDuration - (durationDecreasePerDifficultyLevel * liveDifficultyLevel);

            if (interval < intervalFloor)
            {
                interval = intervalFloor;
            }

            if (duration < durationFloor)
            {
                duration = durationFloor;
            }
        }

        private void StopPlayingSong()
        {
            musicSource.Stop();
        }

        private void PlayGameOverSoundOnce()
        {
            int soundToPlayId = GameManager.instance.GameRandom.Next() % gameOverSounds.Count;

            musicSource.PlayOneShot(gameOverSounds[soundToPlayId], gameOverVolume);
        }

        // runGame starts the game by activating a button every interval*seconds
        private IEnumerator RunGame()
        {
            while (true)
            {
                float randomInterval = GetRandomInterval();
                yield return new WaitForSeconds(randomInterval);
                caloriesBurnedLogger.AddTempValue(GetCaloriesBurned(randomInterval));
                UpdateLiveDifficulty();
                AdjustGameDifficulty();
                StartCoroutine(React());
            }
        }

        // react activates a button, then waits for duration*seconds, then deactivates it again
        private IEnumerator React()
        {
            List<ReactionTrainerButton> notActiveButtons = buttons.FindAll((b) => b.IsActive != true);

            if (notActiveButtons.Count <= 0)
            {
                yield break;
            }

            Debug.Log("Not active button count is " + notActiveButtons.Count);

            int selectedId = GameManager.instance.GameRandom.Next() % notActiveButtons.Count;

            ReactionTrainerButton btn = notActiveButtons[selectedId];

            Debug.Log("Selected button was " + selectedId);

            // linkedID is a hack to not make button activate in the meantime and make the player lose score unintentionally
            int linkedID = spawnId++;

            btn.Activate(linkedID);

            yield return new WaitForSeconds(duration);

            // If the button has been hit by the player in the meantime,
            // or if the button has changed ID in the meantime,
            // we break out of the Enumerator
            if (!btn.IsActive || btn.currentActivatedID != linkedID)
            {
                yield break;
            }

            // The player missed, so we add the reaction that it was not hit within timeframe
            reactionTimeLogger.AddReaction(Time.realtimeSinceStartup - btn.ActivatedTime);

            btn.Deactivate();

            btn.HandleButtonError();

            bool alive = HandleLifeLost();

            if (alive)
            {
                // Disable collision to not get double minus if the user accidentally hit this just as it deactivates because it feels like shit
                btn.DisableCollision();
                // Wait for duration. Could ideally be duration-timeSpentActive works fine.
                yield return new WaitForSeconds(this.interval * 0.5f);

                // if the object has been destroyed in the meantime
                if (this == null)
                {
                    yield break;
                }
                // Re-enable collision after duration is done
                btn.EnableCollision();
            }
        }

        private bool HandleLifeLost()
        {
            if (godMode)
            {
                return true;
            }

            this.currentLives -= 1;

            if (this.currentLives > 0)
            {
                Debug.Log("LOST LIFE: " + this.currentLives);
                this.timer.SetTimerText("❤️" + this.currentLives.ToString());
                return true;
            }
            else
            {
                Debug.Log("GAME OVER");
                GameOver();
                return false;
            }
        }

        public override void GameOver()
        {
            // NOTE: This manager does not utilize base.GameOver() however the other classes do, make sure this
            // class mimics the same functionality // TODO: Look into a better way of solving this later
            // potentially having another function in base class that gets called in all managers at GameOver()
            // or do something like GameOver(bool changeEnabledObjects = true) to the base function?
            _playing = false;

            DisableButtonCollisions();
            timer.SetTimerText("GAME OVER!");

            Debug.Log(LOG_PREFIX + " your score was: " + score.ToString());

            scoreLogger.SetValue(score);
            Metrics.AchievementManager.instance.UpdateProgressData("Score", score);
            Metrics.AchievementManager.instance.UpdateProgressData("RTT_Duration", (int)GameDuration);

            StopCoroutine(this.gameRoutine);
            StopPlayingSong();
            PlayGameOverSoundOnce();

            Monitoring.SessionManager.instance.ActivityCompleted();

            // Start displaying menu and disabling game objects after a few milliseconds
            StartCoroutine(DisableGameObjectsAndEnableGameMenu(0.5f));
            //this.StartGame();
        }

        private IEnumerator DisableGameObjectsAndEnableGameMenu(float seconds)
        {
            yield return new WaitForSecondsRealtime(seconds);

            // if object has been broken in the meantime
            if (this == null)
            {
                yield break;
            }
            DeactivateButtons();
            gamePlayObjects.SetActive(false);
            gameMenu.gameObject.SetActive(true);
        }

        // disableButtonCollisions disables collisions for all buttons
        private void DisableButtonCollisions()
        {
            foreach (ReactionTrainerButton btn in this.buttons)
            {
                btn.DisableCollision();
            }
        }

        private void DeactivateButtons()
        {
            foreach (ReactionTrainerButton btn in this.buttons)
            {
                btn.Deactivate();
            }
        }

        // enableButtonCollisions enables collisions for all buttons
        private void EnableButtonCollisions()
        {
            foreach (ReactionTrainerButton btn in this.buttons)
            {
                btn.EnableCollision();
            }
        }

        // ======================= Button events =======================

        public void HandleButtonHit(ReactionTrainerButton btn, ReactionTrainerButton.Events eventType)
        {
            switch (eventType)
            {
                case ReactionTrainerButton.Events.ACTIVE_BUTTON_HIT:
                    StartCoroutine(this.OnActiveButtonHit(btn));
                    break;

                case ReactionTrainerButton.Events.NONACTIVE_BUTTON_HIT:
                    StartCoroutine(this.OnNonActiveButtonHit(btn));
                    break;

                default:
                    break;
            }
        }

        // onActivateButtonHit eventlister which fires when a green/active button is hit
        private IEnumerator OnActiveButtonHit(ReactionTrainerButton btn)
        {
            // Only register hit once by disabling collision
            btn.DisableCollision();

            // Logging that the player hit the button and how long it took
            reactionTimeLogger.AddReaction(Time.realtimeSinceStartup - btn.ActivatedTime);

            // Logging that the player hit a button
            hitLogger.Add(1);

            score += (int)(liveDifficultyLevel * scoreModifier);
            scoreText.text = this.score.ToString();

            // Wait for duration. Could ideally be duration-timeSpentActive works fine.
            yield return new WaitForSeconds(this.interval * 0.5f);

            // if the object has been destroyed in the meantime
            if (this == null)
            {
                yield break;
            }
            // Re-enable collision after duration is done
            btn.EnableCollision();
        }

        // onNonActiveButtonhit eventlistener which fires when a red/non-active button is hit
        private IEnumerator OnNonActiveButtonHit(ReactionTrainerButton btn)
        {
            Debug.Log("HIT NON ACTIVE " + btn.name);
            // Only register hit once by disabling collision
            btn.DisableCollision();

            bool isAlive = HandleLifeLost();

            if (!isAlive)
            {
                yield break;
            }

            this.scoreText.text = this.score.ToString();

            yield return new WaitForSeconds(this.interval * 0.5f);
            // if the object has been destroyed in the meantime
            if (this == null)
            {
                yield break;
            }

            // Re-enable collision after duration is done
            btn.EnableCollision();
        }

        // initSceneGameObjects finds and sets required gameobjects if they are not set in editor
        // returns false if objects are not present in scene
        private bool InitSceneGameObjects()
        {
            // If no timer is set in editor, try to find it
            if (this.timer == null)
            {
                // Find timer object in the scene
                GameObject timer = GameObject.Find(TIMER_OBJECT_NAME);
                if (timer == null)
                {
                    Debug.LogError(LOG_PREFIX + " no timer in scene");
                    return false;
                }

                // Timer object in scene must have timer script attached
                this.timer = timer.gameObject.GetComponent<Timer>();
                if (this.timer == null)
                {
                    Debug.LogError(LOG_PREFIX + " timer object doesn't hold timer component");
                    return false;
                }
            }

            // If no scoreText is set in editor, try to find it
            if (this.scoreText == null)
            {
                GameObject score = GameObject.Find(SCORE_OBJECT_NAME);
                if (timer == null)
                {
                    Debug.LogError(LOG_PREFIX + " no score in scene");
                    return false;
                }

                this.scoreText = score.GetComponentInChildren<Text>();
                if (this.scoreText == null)
                {
                    Debug.LogError(LOG_PREFIX + " score object doesn't hold text component");
                    return false;
                }
            }

            return true;
        }

        // ======================= Timer events =======================

        public void HandleTimer(Timer.Events eventType)
        {
            switch (eventType)
            {
                case Timer.Events.TIMER_FINISH:
                    StartCoroutine(OnTimerFinish());
                    break;

                case Timer.Events.TIMER_START:
                    this.OnTimerStart();
                    break;

                default:
                    break;
            }
        }

        // onTimerFinish starts the game and edits the timer text when it ends
        private IEnumerator OnTimerFinish()
        {
            this.EnableButtonCollisions();

            StartCoroutine(this.gameRoutine);
            this.timer.SetTimerText("GO");

            // Start playing song because game has started
            StartPlayingRandomSong();

            yield return new WaitForSeconds(1.0f);
            this.timer.SetTimerText("❤️" + this.currentLives.ToString());
        }

        // onTimerStart eventlistener to TIMER_START event
        private void OnTimerStart()
        {
            Debug.Log(LOG_PREFIX + " timer started");
        }

        // Defines minigame behavour when paused
        [ContextMenu("Pause")] // Mainly for testing
        public void Pause()
        {
            if (this.gameRoutine == null)
            {
                Debug.LogWarning(LOG_PREFIX + " pause was called but game never initialized");
                return;
            }

            if (this.paused)
            {
                Debug.LogWarning(LOG_PREFIX + " pause was called but game is already paused");
                return;
            }

            this.paused = true;

            this.timer.SetTimerText("Paused");

            StopCoroutine(this.gameRoutine);
        }

        // Defines minigame behavour when resuming after pause
        [ContextMenu("Resume")] // Mainly for testing
        public void Resume()
        {
            if (this.gameRoutine == null)
            {
                Debug.LogWarning(LOG_PREFIX + " resume was called but game never initialized");
                return;
            }

            if (!this.paused)
            {
                Debug.LogWarning(LOG_PREFIX + " resume called but game is already running");
                return;
            }

            this.paused = false;

            this.timer.SetTimerText("");

            StartCoroutine(this.gameRoutine);
        }

        // Fetches all the Button components in the scene
        [ContextMenu("Set Buttons")]
        private void SetButtons()
        {
            buttons = new List<ReactionTrainerButton>();
            GameObject panel = GameObject.Find(BUTTON_PANEL_OBJECT_NAME);
            if (panel == null)
            {
                Debug.LogError(LOG_PREFIX + " no button panel in scene");
                return;
            }

            foreach (Transform child in panel.transform)
            {
                ReactionTrainerButton btn = child.gameObject.GetComponent<ReactionTrainerButton>();
                if (btn == null)
                {
                    continue;
                }

                buttons.Add(btn);
            }
        }
    }
}