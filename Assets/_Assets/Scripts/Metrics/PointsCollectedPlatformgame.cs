﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace HWHub.Metrics
{
    public class PlatformPointsMetric : Metric
    {
        public new delegate void ValueChangedHandler(object sender, MetricEventArgs<int> e);
        public new static ValueChangedHandler ValueChanged;

        protected override void OnValueChanged() { ValueChanged(this, new MetricEventArgs<int>(value)); }

    }
}
