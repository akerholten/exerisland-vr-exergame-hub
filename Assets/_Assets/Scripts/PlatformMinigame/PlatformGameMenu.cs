﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Valve.VR;

namespace HWHub.PlatformGame
{
    // TODO: rename to miniGameMenu? because it now will be a general minigame menu
    public class PlatformGameMenu : MonoBehaviour
    {
        [SerializeField] private Text statusText;
        [SerializeField] private Text playButtonText;
        [SerializeField] private MiniGameManager miniGameManager;
        [SerializeField] private List<Button> difficultyButtons;

        private void OnEnable()
        {
            if (miniGameManager == null)
                miniGameManager = FindObjectOfType<MiniGameManager>();

            if (GameManager.instance == null)
            {
                Debug.LogError("GameManager.instance was null when enabling game menu");
            }

            GameManager.instance.ActivateLaserPointer();

            if (miniGameManager.HasGameBeenPlayedAndCompleted())
            {
                statusText.text = "Game Completed!" + System.Environment.NewLine + "Score: " + miniGameManager.score;
                UpdateDifficultyButtonColors(miniGameManager.difficultyLevel);
                playButtonText.text = "Play again";
            }
            else // Game has not been played yet
            {
                statusText.text = miniGameManager.GameExplanationText;
                UpdateDifficultyButtonColors(miniGameManager.difficultyLevel);
                playButtonText.text = "Play";
            }
        }

        private void UpdateDifficultyButtonColors(int difficulty)
        {
            for (int i = 0; i < difficultyButtons.Count; i++)
            {
                if (i == difficulty - 1)
                {
                    difficultyButtons[i].image.color = difficultyButtons[i].colors.selectedColor;
                }
                else
                {
                    difficultyButtons[i].image.color = difficultyButtons[i].colors.normalColor;
                }
            }
        }

        public void StartGame()
        {
            GameManager.instance.DeActivateLaserPointer();
            miniGameManager.StartGame();
            gameObject.SetActive(false); // Set this menu to not active right now
        }

        public void SetDifficulty(int difficulty)
        {
            miniGameManager.difficultyLevel = difficulty;

            UpdateDifficultyButtonColors(difficulty);
        }

        public void BackToMainHub()
        {
            if (miniGameManager.IsPlaying)
            {
                miniGameManager.GameOver();
            }
            GameManager.instance.DeActivateLaserPointer();

            SteamVR_LoadLevel.Begin(Constants.Scenes.MAIN_HUB, showGrid: true, fadeOutTime: 0.75f);
            //SceneManager.LoadScene(Constants.Scenes.MAIN_HUB, LoadSceneMode.Single);
        }
    }
}