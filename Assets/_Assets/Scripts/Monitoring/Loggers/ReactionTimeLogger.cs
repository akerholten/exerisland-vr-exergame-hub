﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace HWHub.Monitoring
{
    public class ReactionTimeLogger : MetricLogger
    {
        private static int SECONDS_TO_MILLISECONDS = 1000;

        protected int totalCountedReactions = 0;
        protected int totalReactionTime = 0;

        public void AddReaction(float seconds)
        {
            totalCountedReactions += 1;
            totalReactionTime += (int)(seconds * SECONDS_TO_MILLISECONDS);
        }

        protected override void OnLoggingStarted()
        {
            totalCountedReactions = 0;
            totalReactionTime = 0;
        }

        protected override void OnLoggingStopped()
        {
            if (totalCountedReactions > 0)
            {
                currentMetric.value = totalReactionTime / totalCountedReactions;
            }
        }
    }
}