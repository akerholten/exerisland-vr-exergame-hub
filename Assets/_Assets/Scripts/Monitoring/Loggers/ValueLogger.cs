﻿using HWHub.Player;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace HWHub.Monitoring
{
    public class ValueLogger : MetricLogger
    {
        // Total int value is kept in currentMetric.value at all times

        protected override void OnLoggingStarted()
        {
            currentMetric.value = 0;
        }

        protected override void OnLoggingStopped()
        {
        }

        public void SetValue(int newValue)
        {
            currentMetric.value = newValue;
        }

        public void Add(int value)
        {
            currentMetric.value += value;
        }
    }
}