﻿using UnityEngine;
using UnityEngine.UI;
using Valve.VR;

namespace HWHub.UI
{
    public class LaserPointer : MonoBehaviour
    {
        [SerializeField] private LineRenderer visualLaser;

        [Range(0, 10)]
        [SerializeField] private float maxRayLength = 5.0f;

        [SerializeField] private LayerMask UI_layerMask;

        public SteamVR_Action_Boolean InteractWithUI_Action = SteamVR_Input.GetAction<SteamVR_Action_Boolean>("InteractUI");

        [SerializeField] private bool active = false;
        private Button previousButton = null;

        // TODO: Consider Graphic Raycaster vs Physics Raycaster, dependent on what we want to do later on
        private void Awake()
        {
            UI.MainMenu.MainMenuEvent -= HandleMainMenuEvent;
            UI.MainMenu.MainMenuEvent += HandleMainMenuEvent;
        }

        private void OnDestroy()
        {
            UI.MainMenu.MainMenuEvent -= HandleMainMenuEvent;
        }

        private void Update()
        {
            if (active)
            {
                previousButton = RaycastUI();
            }
        }

        public void HandleMainMenuEvent(UI.MainMenu.Events eventType)
        {
            if (eventType == UI.MainMenu.Events.EXIT_MENU)
            {
                // If another object than the player has activated the laserpointer, it should stay active
                if (GameManager.instance.IsLaserActivatedByOtherObject)
                {
                    return;
                }
                DeActivate();
            }
            else if (eventType == UI.MainMenu.Events.OPEN_MENU)
            {
                Activate();
            }
        }

        [ContextMenu("Activate")]
        public void Activate()
        {
            active = true;
            if (visualLaser == null) visualLaser = GetComponentInChildren<LineRenderer>();
            visualLaser.gameObject.SetActive(true);
        }

        [ContextMenu("DeActivate")]
        public void DeActivate()
        {
            active = false;
            OnDeHover();
            previousButton = null;
            if (visualLaser == null) visualLaser = GetComponentInChildren<LineRenderer>();
            visualLaser.gameObject.SetActive(false);
        }

        private Button RaycastUI()
        {
            RaycastHit hitInfo = new RaycastHit();
            visualLaser.SetPosition(0, transform.position); // For the visual gameObject laser start position
            visualLaser.SetPosition(1, transform.position + transform.forward * maxRayLength); // If raycast does not hit anything, the laser reaches its max length

            Button buttonHit = null;

            if (Physics.Raycast(transform.position, transform.forward, out hitInfo, maxRayLength, UI_layerMask))
            {
                visualLaser.SetPosition(1, hitInfo.point); // The visual laser only reaches where it hit something

                buttonHit = hitInfo.collider.GetComponent<Button>();
            }

            if (previousButton != null && buttonHit != previousButton)
                OnDeHover();
            if (buttonHit != null)
                OnHoveringButton(buttonHit);

            return buttonHit;
        }

        private void OnHoveringButton(Button button)
        {
            if (button.image.color != button.colors.selectedColor) // Hack to mitigate issues with color changes when selecting a button TODO: resolve
                button.image.color = button.colors.highlightedColor;

            if (InteractWithUI_Action.GetState(SteamVR_Input_Sources.Any)) // Possibly use Hand input source only?
            {
                button.onClick.Invoke();
                Debug.Log("Called buttons OnClick()");
            }
        }

        private void OnDeHover()
        {
            // TODO: Consider that a button might not have image but just a texture if physical object?
            if (previousButton != null && previousButton.image.color != previousButton.colors.selectedColor) // Hack to mitigate issues with color changes when selecting a button TODO: resolve
                previousButton.image.color = previousButton.colors.normalColor;
        }
    }
}