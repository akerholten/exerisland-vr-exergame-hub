﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace HWHub.Metrics
{
    [CreateAssetMenu(fileName = "NewAchievement", menuName = "New Achievement", order = 1)]
    public class Achievement : ScriptableObject
    {
        public Sprite image;
        public string title, description, progressText;
        public int progress, goal;
        public string progressID;

        //ID
        public bool Completed => (progress >= goal);

        //public bool Completed = false;
        //public uint MetricID;
    }
}