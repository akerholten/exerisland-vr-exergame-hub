﻿using HWHub.Metrics;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace HWHub.UI
{
    public class MessagePopup : MonoBehaviour
    {
        [SerializeField] GameObject achievementUI_Prefab;
        [SerializeField] Text infoText;
        [SerializeField] Transform popupParent;
        [SerializeField] GameObject canvasObject;

        public static MessagePopup instance = null; // Singleton instance

        private void Awake()
        {
            if (instance == null) // Singleton instance
            {
                instance = this;
            }
        }

        private void TearDownUI()
        {
            foreach(Transform child in popupParent)
            {
                Destroy(child.gameObject);
            }
        }

        public void DisplayAchievementUnlocked(Achievement achievement)
        {
            canvasObject.SetActive(true);
            TearDownUI();

            infoText.text = "Achievement Unlocked!";
            GameObject achievementUI = Instantiate(achievementUI_Prefab, popupParent);
            achievementUI.GetComponent<AchievementUI>().BuildUI(achievement);

            StartCoroutine(DisableUIAfterSeconds(4.0f));
        }

        private IEnumerator DisableUIAfterSeconds(float seconds)
        {
            yield return new WaitForSeconds(seconds);

            TearDownUI();
            canvasObject.SetActive(false);
        }
    }
}