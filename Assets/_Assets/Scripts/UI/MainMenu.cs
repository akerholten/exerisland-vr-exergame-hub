﻿using HWHub.Player;
using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Valve.VR;

namespace HWHub.UI
{
    // TODO: Add "Are you sure?"-menus to certain options, like quitting etc'
    public class MainMenu : MonoBehaviour
    {
        [SerializeField] private GameObject canvasObject;
        [SerializeField] private Button MainHubButton;
        [SerializeField] private GameObject tabs, mainMenuView, achievementsView, exitApplicationView; // Could possibly be a list of "tabs" instead

        public bool MenuIsActive => canvasObject.activeSelf;

        public enum Events
        {
            OPEN_MENU,
            EXIT_MENU
        }

        public static event Action<Events> MainMenuEvent;

        private void Start()
        {
            MainMenuEvent -= HandleMenuEvent;
            MainMenuEvent += HandleMenuEvent;
        }

        private void OnDestroy()
        {
            MainMenuEvent -= HandleMenuEvent;
        }

        public void TriggerMainMenuEvent(Events type)
        {
            MainMenuEvent(type); // Don't know if I like this too much, probably the event should have been in the HWPlayer.cs maybe :thinking:
        }

        public void HandleMenuEvent(Events type)
        {
            if (type == Events.EXIT_MENU)
            {
                canvasObject.SetActive(false);
            }
            else if (type == Events.OPEN_MENU)
            {
                OpenMenu();
            }
        }

        public void OpenMenu()
        {
            canvasObject.SetActive(true);

            if (SceneManager.GetActiveScene().name == Constants.Scenes.MAIN_HUB) // Doing a check in case we are already inside the default hub, then we don't want to show this button
            {
                MainHubButton.gameObject.SetActive(false);
            }
            else
            {
                MainHubButton.gameObject.SetActive(true);
            }
        }

        private void DisableAllTabs()
        {
            foreach (Transform tab in tabs.transform) // For all children
            {
                tab.gameObject.SetActive(false);
            }
        }

        #region MenuButtons

        public void GoToMainHub()
        {
            MiniGameManager manager = FindObjectOfType<MiniGameManager>();

            if (manager != null && manager.IsPlaying)
            {
                manager.GameOver();
            }

            SteamVR_LoadLevel.Begin(Constants.Scenes.MAIN_HUB, showGrid: true, fadeOutTime: 0.75f);

            //SceneManager.LoadScene(Constants.Scenes.MAIN_HUB, LoadSceneMode.Single);
        }

        public void OpenAchievementsView()
        {
            DisableAllTabs();

            achievementsView.SetActive(true);
        }

        public void OpenMainMenuView()
        {
            DisableAllTabs();

            mainMenuView.SetActive(true);
        }

        public void OpenExitApplicationView()
        {
            DisableAllTabs();

            exitApplicationView.SetActive(true);
        }

        public void CloseMenu()
        {
            MainMenuEvent(Events.EXIT_MENU);
        }

        public void QuitGame()
        {
            // saving and uploading session to db
            // This is now no longer required, as the session is updated throughout the lifespan of the app whenever
            // an activity is completed

            //Monitoring.SessionManager.instance.VerifySessionAndUpload();
            Application.Quit();

            #endregion MenuButtons
        }
    }
}