﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

namespace HWHub.Monitoring
{
    public class ActivityArgs : EventArgs
    {
        public string minigameID;

        public ActivityArgs(string minigameID)
        {
            this.minigameID = minigameID;
        }
    }

    public class SessionManager : MonoBehaviour
    {
        private Session currentSession;
        private string sessionID = "";
        private Activity currentActivity;
        private bool hasActivity = false;
        private List<MetricLogger> metricLoggers;

        public bool HasDataToUpload => (currentSession != null && currentSession.activities != null && currentSession.activities.Count > 0);

        public bool sessionDataUploaded { get; private set; } = false;

        public bool currentlyUploading { get; private set; } = false;

        public static SessionManager instance = null; // Singleton instance

        public delegate void ActivityStartedHandler(object sender, ActivityArgs e);

        public static ActivityStartedHandler ActivityStarted;

        private void Awake()
        {
            if (instance == null)
            {
                instance = this;
                InitNewSession();
            }
            else if (instance != this)
            {
                Destroy(gameObject);
            }
            DontDestroyOnLoad(this); // We want the singleton object to persist through scenes
        }

        public void InitNewSession()
        {
            currentSession = new Session();
            currentActivity = null;
        }

        public void StartNewActivity(string minigameID)
        {
            if (hasActivity) // if we previously had an activity we have not uploaded yet
            {
                Debug.LogWarning("An activity was not dealt with before a new one started");
                ActivityCompleted();
            }
            currentActivity = new Activity(minigameID);
            hasActivity = true;
            sessionDataUploaded = false;
            metricLoggers = new List<MetricLogger>();

            // Calling that the event has started
            ActivityStarted(this, new ActivityArgs(minigameID));
        }

        public void SubscribeLogger(MetricLogger logger)
        {
            metricLoggers.Add(logger);
        }

        public void AddMetricToCurrentActivity(Metric metric)
        {
            currentActivity.metrics.Add(metric);
        }

        public void ActivityCompleted()
        {
            Debug.Log("Called activity completed");

            Debug.Log("Current activity is: " + currentActivity ?? "null");
            if (currentActivity != null)
            {
                foreach (var logger in metricLoggers)
                {
                    AddMetricToCurrentActivity(logger.ReportMetricAndStopLogging());
                }
                currentSession.activities.Add(currentActivity);
            }

            currentActivity = null;
            hasActivity = false;

            VerifySessionAndUpload();
        }

        public void VerifySessionAndUpload()
        {
            if (HasDataToUpload && !sessionDataUploaded && !currentlyUploading)
            {
                StartCoroutine(UploadSessionToDB());
            }
        }

        public IEnumerator UploadSessionToDB()
        {
            currentlyUploading = true;

            // For updating the top-level "duration" value in json object before sent out
            currentSession.UpdateDuration();

            string jsonRequest = JsonUtility.ToJson(currentSession);

            Debug.Log("Sent a session update");
            Debug.Log(jsonRequest);
            byte[] jsonReqData = System.Text.Encoding.UTF8.GetBytes(jsonRequest);

            UnityWebRequest req = new UnityWebRequest();

            req.method = "POST";
            req.uploadHandler = new UploadHandlerRaw(jsonReqData);
            req.downloadHandler = new DownloadHandlerBuffer();

            req.SetRequestHeader("Personal-ID", GameManager.instance.UserID);
            req.SetRequestHeader("Content-Type", "application/json; charset=utf-8");

            // If the session has not been initialized / uploaded before
            if (sessionID == "")
            {
                req.url = Constants.BACKEND_URL + Constants.Paths.UploadSession;
            }
            else // Session has been uploaded before, so we just update the existing session
            {
                req.url = Constants.BACKEND_URL + Constants.Paths.UpdateExistingSession;
                req.SetRequestHeader("Session-ID", sessionID);
            }

            yield return req.SendWebRequest();

            if (req.isNetworkError)
            {
                Debug.Log("Error while sending session: " + req.error);
            }
            else
            {
                Debug.Log("Received when uploading session: " + req.downloadHandler.text);

                // If sessionID is not stored from backend yet, we do it now, basically means that it is the first push of the session
                if (sessionID == "")
                {
                    sessionID = req.downloadHandler.text;
                }
                sessionDataUploaded = true;
            }
            currentlyUploading = false;
        }
    }
}