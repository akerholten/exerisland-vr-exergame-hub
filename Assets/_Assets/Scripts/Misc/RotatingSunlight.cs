﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace HWHub
{
    public class RotatingSunlight : MonoBehaviour
    {
        [SerializeField] private int minutesPerFullRotation;
        private float rotationAmountPerSecond;

        // Start is called before the first frame update
        private void Start()
        {
            rotationAmountPerSecond = 360 / (minutesPerFullRotation * 60);
        }

        // Update is called once per frame
        private void FixedUpdate()
        {
            transform.rotation *= Quaternion.Euler(0, rotationAmountPerSecond * Time.fixedDeltaTime, 0);
        }
    }
}