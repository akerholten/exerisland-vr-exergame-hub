﻿using System;
using System.Collections.Generic;

namespace HWHub.Monitoring
{
    [Serializable]
    public class Session
    {
        public string duration;
        public string createdAt;
        public List<Activity> activities;

        public Session()
        {
            activities = new List<Activity>();
        }

        public void UpdateDuration()
        {
            int durationInt = 0;
            foreach (var activity in activities)
            {
                Metric durationMetric = activity.metrics.Find((e) => e.id == "Duration");
                if (durationMetric != null)
                {
                    durationInt += durationMetric.value;
                }
            }

            duration = durationInt.ToString();
        }
    }

    [Serializable]
    public class Activity
    {
        public string minigameID;
        public List<Metric> metrics;

        public Activity()
        {
            this.metrics = new List<Metric>();
        }

        public Activity(string minigameID)
        {
            this.minigameID = minigameID;
            this.metrics = new List<Metric>();
            //this.createdAt = DateTime.Now.ToString("yyyy-MM-dd'T'HH:mm:ss.fffzzz", DateTimeFormatInfo.InvariantInfo);
        }
    }

    [Serializable]
    public class Metric
    {
        public string id;
        public string name;
        public int value;
        public string unit;

        public Metric()
        {
        }

        public Metric(string id, string name, int value, string unit)
        {
            this.id = id;
            this.name = name;
            this.value = value;
            this.unit = unit;
        }
    }
}