﻿using HWHub.Metrics;
using HWHub.Player;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace HWHub.UI
{
    public class AchievementUI : MonoBehaviour
    {
        [SerializeField] private Text title, description, progressText;
        [SerializeField] private Slider progressSlider;
        [SerializeField] private GameObject completedCheckbox;
        [SerializeField] private Image image;

        //public Achievement achievement; //Maybe want to have this

        public void BuildUI(Achievement achievement)
        {
            title.text = achievement.title;
            description.text = achievement.description;
            progressText.text = achievement.progress.ToString() + " / " + achievement.goal.ToString() + " " + achievement.progressText;
            progressSlider.value = (float)achievement.progress / (float)achievement.goal;
            image.sprite = achievement.image;

            if (achievement.Completed)
                completedCheckbox.SetActive(true);
        }
    }
}
