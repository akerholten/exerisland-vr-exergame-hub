﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Random = System.Random;

namespace HWHub.PlatformGame
{
    public class PlatformGameManager : MiniGameManager
    {
        public override string MinigameID => "Platform_Minigame";
        public override string GameExplanationText => "Hit the green objects." + System.Environment.NewLine + "Dodge the red ones." + System.Environment.NewLine + "Good luck!";

        protected override int CalorieDifficultyLevelTop => 4;

        // Estimate from table-tennis "light" at https://www.kalkuler.com/kalkulator/trening/treningskalkulator/
        protected override int MinCaloriesBurnedPerHour => 222;

        [Header("Platform Game Manager Info")]
        public int hittablesHit = 0;

        public int obstaclesHit = 0;
        //public string minigameID = "Platform_Minigame";

        #region GameSettingsVariables

        [Header("Game Settings")]
        public float gameSpeed = 1.0f;  // Gamespeed can be changed to alter difficulty level

        // time in seconds // this should be default, but if there is a song with x length, gamelength should be set to that
        public float gameLength = 60.0f;

        public float defaultGameLength = 60.0f;

        public float spawnInterval = 2.0f;  // default spawn interval time in seconds
        private float intervalFloor = 0.1f;

        public float CurrentSpawnInterval => spawnInterval / gameSpeed;
        private int randomIntervalSteps = 100;

        private float speedIncreasePerDifficulty = 0.90f;

        private float minIntervalDecreasePerDifficulty = 0.06f; // can maximum be 0.075 ish or else it will go below 0

        public float timeLasted = 0.0f;  // Can be edited by gameSpeed, and thus is not an accurate estimate of time played
        public bool startGameOnLoad = true;
        public bool spawningIsOver = false;
        public string gameSeed;
        private int actualGameSeed;
        public bool useStringSeed = false;
        [SerializeField] private GameObject movingBlockPrefab;
        [SerializeField] private List<GameObject> obstaclePrefabs;
        [SerializeField] private List<GameObject> hittablePrefabs;
        private GameObject lastBlock = null;
        private float lastTime = 0.0f;
        public Transform blockStartPos, blockMiddlePos, blockEndPos;

        #endregion GameSettingsVariables

        [Header("Object references")]

        // TODO: Could potentially look into hooking almost all loggers up as "ValueLoggers", e.g. Duration also, but its fine for now

        #region Loggers

        [SerializeField] private Monitoring.ValueLogger scoreLogger;

        [SerializeField] private Monitoring.ValueLogger hitLogger;
        [SerializeField] private Monitoring.ValueLogger obstacleLogger;
        [SerializeField] private Monitoring.FloatLogger caloriesBurnedLogger;

        #endregion Loggers

        [SerializeField] private Text scoreText;

        private Random rand;

        public delegate void PlayerHitObjectHandler(object sender, HitEventArgs e);

        // Might need to -= this when leaving the game mid-game or something? TOOD: potential bug so look into it
        public static PlayerHitObjectHandler PlayerHitObject;

        private void Awake()
        {
            AdjustGameDifficulty(); // Fuzzy logic to adjust difficulty with game speed
            // Delegate event for when player is hitting obstacles or hittables
            PlayerHitObject -= OnPlayerHitObject;
            PlayerHitObject += OnPlayerHitObject;

            if (gameMenu == null)
                gameMenu = FindObjectOfType<PlatformGameMenu>();

            if (musicSource == null)
            {
                musicSource = GetComponent<AudioSource>();
            }

            VerifyLoggers();

            gameMenu.gameObject.SetActive(true); // Boot game menu
        }

        private void OnDestroy()
        {
            // Desubscribe to events to avoid null references if exiting game and joining again
            PlayerHitObject -= OnPlayerHitObject;
        }

        protected override void VerifyLoggers()
        {
            // TODO: rewrite this into using a function VerifyLogger(ValueLogger valueLogger, string metricID) or something
            // TODO: generally rewrite this piece of garbage......... really inefficient and redundant way of doing this..
            if (scoreLogger == null || scoreLogger.metricId != "Score")
            {
                foreach (var logger in FindObjectsOfType<Monitoring.ValueLogger>())
                {
                    if (logger.metricId == "Score")
                    {
                        scoreLogger = logger;
                        break;
                    }
                }

                if (scoreLogger == null)
                {
                    Debug.LogError("scoreLogger was null and was not found in scene!");
                }
            }

            if (hitLogger == null || hitLogger.metricId != "Hittable_Hits")
            {
                foreach (var logger in FindObjectsOfType<Monitoring.ValueLogger>())
                {
                    if (logger.metricId == "Hittable_Hits")
                    {
                        hitLogger = logger;
                        break;
                    }
                }

                if (hitLogger == null)
                {
                    Debug.LogError("hitLogger was null and was not found in scene!");
                }
            }

            if (obstacleLogger == null || obstacleLogger.metricId != "Obstacle_Not_Dodged")
            {
                foreach (var logger in FindObjectsOfType<Monitoring.ValueLogger>())
                {
                    if (logger.metricId == "Obstacle_Not_Dodged")
                    {
                        obstacleLogger = logger;
                        break;
                    }
                }

                if (obstacleLogger == null)
                {
                    Debug.LogError("obstacleLogger was null and was not found in scene!");
                }
            }

            if (caloriesBurnedLogger == null || caloriesBurnedLogger.metricId != "Calories_Burned")
            {
                foreach (var logger in FindObjectsOfType<Monitoring.FloatLogger>())
                {
                    if (logger.metricId == "Calories_Burned")
                    {
                        caloriesBurnedLogger = logger;
                        break;
                    }
                }

                if (caloriesBurnedLogger == null)
                {
                    Debug.LogError("caloriesBurnedLogger was null and was not found in scene!");
                }
            }
        }

        public void OnPlayerHitObject(object sender, HitEventArgs e)
        {
            if (e.hitType == HitType.HITTABLE)
            {
                score += e.score;
                hittablesHit++;
                hitLogger.Add(1);
                Metrics.AchievementManager.instance.UpdateProgressData("Hittable_Hits", 1);
            }
            else if (e.hitType == HitType.OBSTACLE)
            {
                score -= e.score;
                obstacleLogger.Add(1);
                obstaclesHit++;
            }

            scoreLogger.SetValue(score); // Might be inefficient and could potentially be done in GameOver() instead
            scoreText.text = "Score: " + score.ToString();
        }

        #region GameRunningLogic

        private void AdjustGameDifficulty()
        {
            // Difficulty level starts at 1 and we do not want to adjust at that level
            gameSpeed = 1.0f + ((difficultyLevel - 1) * speedIncreasePerDifficulty); // Fuzzy logic to adjust difficulty with game speed

            liveDifficultyLevel = difficultyLevel;
        }

        [ContextMenu("Start Game")]
        public override void StartGame()
        {
            base.StartGame();
            RestartGameState();
            AdjustGameDifficulty(); // Fuzzy logic to adjust difficulty with game speed

            //if (useStringSeed)
            //{
            //    actualGameSeed = gameSeed.GetHashCode();
            //}
            //else
            //{
            //    actualGameSeed = (int)Time.realtimeSinceStartup;
            //}

            rand = GameManager.instance.GameRandom;

            musicSource.Stop();
            StartPlayingRandomSong();
            gameLength = musicSource.clip.length; // Adjust game length to fit length of the song
            StartCoroutine(SpawnRoutine());
            StartCoroutine(GameTimeCounter());
        }

        private void RestartGameState()
        {
            score = 0;
            hittablesHit = 0;
            obstaclesHit = 0;
            timeLasted = 0;
            spawningIsOver = false;
            lastTime = Time.realtimeSinceStartup;
            gameLength = defaultGameLength;
            scoreText.text = "Score: " + score.ToString();
        }

        private IEnumerator SpawnRoutine()
        {
            PlatformMovingBlock thisBlock;
            while (!spawningIsOver)
            {
                lastBlock = Instantiate(movingBlockPrefab, blockStartPos.position, Quaternion.identity, transform);

                thisBlock = lastBlock.GetComponent<PlatformMovingBlock>();

                // Add hittables/obstacles to the moving blocks before they start moving
                thisBlock.AddObjects(rand, hittablePrefabs, obstaclePrefabs, difficultyLevel);

                // Start moving along the path
                StartCoroutine(thisBlock.MoveAlongPath(this));

                float randomInterval = GetRandomInterval();
                yield return new WaitForSeconds(randomInterval);

                caloriesBurnedLogger.AddTempValue(GetCaloriesBurned(randomInterval));
            }

            while (!IsGameOver())
            {
                thisBlock = lastBlock.GetComponent<PlatformMovingBlock>();
                musicSource.volume = Mathf.Lerp(gameSongVolume, 0.0f, thisBlock.SecondsMoved / thisBlock.SecondsToEndPosition);

                yield return new WaitForFixedUpdate();
            }
        }

        private float GetRandomInterval()
        {
            return Mathf.Max(intervalFloor, CurrentSpawnInterval + ((CurrentSpawnInterval * (0.35f - (minIntervalDecreasePerDifficulty * difficultyLevel))) - ((CurrentSpawnInterval / randomIntervalSteps) * GameManager.instance.GameRandom.Next(randomIntervalSteps))));
        }

        private IEnumerator GameTimeCounter()
        {
            lastTime = Time.realtimeSinceStartup;
            while (!IsGameOver())
            {
                yield return new WaitForFixedUpdate();
                // Could be affected by gameSpeed, but probably should not
                timeLasted += (Time.realtimeSinceStartup - lastTime); //  * gameSpeed
                lastTime = Time.realtimeSinceStartup;

                // TODO: Also display how much time has passed?

                if (timeLasted >= gameLength)
                {
                    spawningIsOver = true;
                }
            }

            // Game is over, deal with saving state and giving feedback of performance to player
            // Provide with menu to retry or go back to main hub?
            GameOver();
        }

        private bool IsGameOver()
        {
            return (timeLasted >= gameLength) && lastBlock == null;
        }

        public override bool HasGameBeenPlayedAndCompleted()
        {
            return hittablesHit != 0 || obstaclesHit != 0;
        }

        #endregion GameRunningLogic
    }
}