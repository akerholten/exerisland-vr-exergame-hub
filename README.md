# ExerIsland Exergame VR Hub

VR hub consisting of several minigames with a purpose to improve specific health issues or general life quality.

Used for my master thesis project, conducted a two-week experiment with this platform. Activities from the VR app was logged and could be displayed in a frontend web application.

The repository for the backend, and frontend used for the remote user study platform can be seen here: [https://gitlab.com/akerholten/vr-health-and-wellness-remote-monitoring](https://gitlab.com/akerholten/vr-health-and-wellness-remote-monitoring)

## Note:
**This specific repository is a duplicate of the actual repository as it includes proprietary assets. Trying to run this project in Unity will cause dependency issues in assets that are not included.**
