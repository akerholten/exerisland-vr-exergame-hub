﻿using UnityEngine;

namespace HWHub.Monitoring
{
    public abstract class MetricLogger : MonoBehaviour
    {
        public string metricId;
        public string metricName;
        public int initialValue;
        public string metricUnit;

        protected Metric currentMetric;
        protected bool active = false;

        private Metric InitiateMetric()
        {
            return new Metric(metricId, metricName, initialValue, metricUnit);
        }

        protected void OnDestroy()
        {
            SessionManager.ActivityStarted -= OnActivityStarted;
        }

        protected void Awake()
        {
            SessionManager.ActivityStarted -= OnActivityStarted;
            SessionManager.ActivityStarted += OnActivityStarted;
        }

        protected void OnActivityStarted(object sender, ActivityArgs e)
        {
            currentMetric = InitiateMetric();
            SessionManager.instance.SubscribeLogger(this);
            active = true;
            OnLoggingStarted();
        }

        public Metric ReportMetricAndStopLogging()
        {
            active = false; // We are done logging, so stop from now

            OnLoggingStopped();

            return currentMetric;
        }

        protected abstract void OnLoggingStarted();

        protected abstract void OnLoggingStopped();
    }
}