﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

namespace HWHub.PlatformGame
{
    public class HitEventArgs : EventArgs
    {
        public HitType hitType;
        public int score;

        public HitEventArgs(HitType hitType, int score)
        {
            this.hitType = hitType;
            this.score = score;
        }
    }

    public enum HitType
    {
        OBSTACLE = -1,
        HITTABLE = 1
    }

    [RequireComponent(typeof(AudioSource))]
    public class PlatformObject : MonoBehaviour
    {
        [SerializeField] private MeshRenderer meshRenderer;
        [SerializeField] private HitType objectType;
        [SerializeField] private int score;
        [SerializeField] private float dissolveSpeed = 1.0f;
        [SerializeField] private float dissolveAmount = 0.0f;
        private bool hit = false;

        [Header("Audio Clip")]
        [Range(0.0f, 1.0f)]
        [SerializeField] private float clipVolume = 1.0f;

        [SerializeField] protected List<AudioClip> audioClips;
        [SerializeField] protected AudioSource audioSource;

        private void Start()
        {
            if (meshRenderer == null)
                meshRenderer = GetComponent<MeshRenderer>();
            if (audioSource == null)
                audioSource = GetComponent<AudioSource>();
        }

        private void OnTriggerEnter(Collider collision)
        {
            if (hit) return;
            if (collision.transform.root.tag == "Player")
            {
                hit = true;
                if (collision.attachedRigidbody.transform.name.Contains("HandColliderRight"))
                {
                    GameManager.instance.Haptic.Execute(0, 0.1f, 30.0f, 1.0f, SteamVR_Input_Sources.RightHand);
                }
                if (collision.attachedRigidbody.transform.name.Contains("HandColliderLeft"))
                {
                    GameManager.instance.Haptic.Execute(0, 0.1f, 30.0f, 1.0f, SteamVR_Input_Sources.LeftHand);
                }
                OnPlayerHit();
            }
        }

        [ContextMenu("Debug Dissolve Object")]
        private void DEBUG_DISSOLVE_OBJECT()
        {
            StartCoroutine(DissolveObject());
        }

        private IEnumerator DissolveObject()
        {
            float lastTime = Time.realtimeSinceStartup;
            while (dissolveAmount < 1.0f)
            {
                yield return new WaitForEndOfFrame();
                dissolveAmount += (Time.realtimeSinceStartup - lastTime) * dissolveSpeed;
                lastTime = Time.realtimeSinceStartup;

                if (dissolveAmount > 1.0f) dissolveAmount = 1.0f;

                meshRenderer.material.SetFloat("_DissolveAmount", dissolveAmount);
            }
        }

        private void OnPlayerHit()
        {
            PlatformGameManager.PlayerHitObject(this, new HitEventArgs(objectType, score));

            // Play sound
            int soundToPlayId = GameManager.instance.GameRandom.Next() % audioClips.Count;

            audioSource.PlayOneShot(audioClips[soundToPlayId], clipVolume);

            // Dissolving object
            StartCoroutine(DissolveObject());
        }
    }
}