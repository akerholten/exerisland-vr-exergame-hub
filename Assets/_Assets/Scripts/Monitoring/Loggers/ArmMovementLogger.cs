﻿using HWHub.Player;
using UnityEngine;

namespace HWHub.Monitoring
{
    public class ArmMovementLogger : MetricLogger
    {
        private float totalDistance = 0;

        private bool initialPosSet = false;
        private Vector3 previousLeftHandPosition;
        private Vector3 previousRightHandPosition;

        protected override void OnLoggingStarted()
        {
            initialPosSet = false;
            totalDistance = 0;
        }

        protected override void OnLoggingStopped()
        {
            Metrics.AchievementManager.instance.UpdateProgressData("Arm_Movement", currentMetric.value);
        }

        private void FixedUpdate()
        {
            if (active)
            {
                if (!initialPosSet)
                {
                    previousLeftHandPosition = HWPlayer.instance.LeftHand.transform.localPosition;
                    previousRightHandPosition = HWPlayer.instance.RightHand.transform.localPosition;
                    initialPosSet = true;
                }

                Vector3 newLeftPos = HWPlayer.instance.LeftHand.transform.localPosition;
                Vector3 newRightPos = HWPlayer.instance.RightHand.transform.localPosition;

                totalDistance += Vector3.Distance(previousLeftHandPosition, newLeftPos);
                totalDistance += Vector3.Distance(previousRightHandPosition, newRightPos);

                previousLeftHandPosition = newLeftPos;
                previousRightHandPosition = newRightPos;

                currentMetric.value = (int)totalDistance;
            }
        }
    }
}