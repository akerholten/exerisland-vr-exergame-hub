﻿using HWHub.PlatformGame;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace HWHub
{
    // Abstract class that declares interface the minigames must follow, gamemenu will use these calls here and
    // the specific game managers will deal with their independent logic themselves
    public abstract class MiniGameManager : MonoBehaviour
    {
        [Header("Minigame Manager Info")]
        public int score;

        public int difficultyLevel;

        public bool IsPlaying => _playing;
        protected bool _playing = false;

        // gameHasBeenPlayed is true if a round of the minigame has previously been played in this session
        protected bool gameHasBeenPlayed = false;

        [SerializeField] protected PlatformGameMenu gameMenu;
        [SerializeField] protected GameObject gamePlayObjects;

        #region Audio

        // The music source playing sound in the scene
        protected AudioSource musicSource;

        // AudioClips and songs
        public List<AudioClip> impactClips;

        public List<AudioClip> gameOverSounds;
        public List<AudioClip> enabledClips;
        public List<AudioClip> successClips;
        public List<AudioClip> errorClips;
        public List<AudioClip> gameSongs;

        // Volumes of different sound clips
        [Range(0.0f, 1.0f)]
        public float impactClipVolume = 0.5f;

        [Range(0.0f, 1.0f)]
        public float enabledClipVolume = 0.5f;

        [Range(0.0f, 1.0f)]
        public float successClipVolume = 0.5f;

        [Range(0.0f, 1.0f)]
        public float errorClipVolume = 0.5f;

        [Range(0.0f, 1.0f)]
        public float gameSongVolume = 0.5f;

        [Range(0.0f, 1.0f)]
        public float gameOverVolume = 0.5f;

        // StartPlayingRandomSong takes a random song from the list of songs and plays it
        protected virtual void StartPlayingRandomSong()
        {
            if (gameSongs == null || gameSongs.Count == 0)
            {
                return;
            }
            musicSource.loop = true;
            musicSource.clip = gameSongs[GameManager.instance.GameRandom.Next() % gameSongs.Count];
            musicSource.volume = gameSongVolume;
            musicSource.Play();
        }

        // PlayRandomSoundFromList takes a random clip from the list of clips and plays it
        protected void PlayRandomSoundFromList(List<AudioClip> clips, float volume)
        {
            int soundToPlayId = GameManager.instance.GameRandom.Next() % clips.Count;

            PlaySoundOnce(clips[soundToPlayId], volume);
        }

        protected void PlaySoundOnce(AudioClip clip, float volume)
        {
            if (musicSource != null)
            {
                musicSource.PlayOneShot(clip, volume);
            }
        }

        #endregion Audio

        // liveDifficultyLevel is used for minigames that have scaling difficulty over time
        protected float liveDifficultyLevel;

        public float GameDuration => Time.realtimeSinceStartup - gameStartTime;

        // gameStartTime is stored when StartGame() is called, used to calculated game duration
        protected float gameStartTime;

        #region CalorieStuff

        public float MinimumCaloriesBurnedPerMinute => MinCaloriesBurnedPerHour / 60;

        public float MinimumCaloriesBurnedPerHour => MinCaloriesBurnedPerHour;

        public float GetCaloriesBurned(float seconds)
        {
            float currentAvgCaloriesBurnedPerHour = Mathf.Lerp(MinCaloriesBurnedPerHour, MinCaloriesBurnedPerHour * _caloriesRange, liveDifficultyLevel / CalorieDifficultyLevelTop);
            float currentAvgCaloriesBurnedPerMinute = currentAvgCaloriesBurnedPerHour / 60;
            float currentAvgCaloriesBurnedPerSecond = currentAvgCaloriesBurnedPerMinute / 60;

            return currentAvgCaloriesBurnedPerSecond * seconds;
        }

        // MinCaloriesBurnedPerHour return an int value of the estimated min kcal burned during gameplay
        protected abstract int MinCaloriesBurnedPerHour { get; }

        // CalorieDifficultyLevelTop returns an value that represents when the maximum calories burned per hour is reached
        protected abstract int CalorieDifficultyLevelTop { get; }

        // _caloriesRange defines how much the calories burned increases from the MinCaloriesBurned level
        protected float _caloriesRange = 1.5f;

        #endregion CalorieStuff

        public abstract string MinigameID { get; }

        // Game Explanation Text is displayed to user through minigame UI
        public abstract string GameExplanationText { get; }

        public virtual bool HasGameBeenPlayedAndCompleted()
        {
            return gameHasBeenPlayed;
        }

        public virtual void StartGame()
        {
            _playing = true;
            gameHasBeenPlayed = true;
            score = 0;
            gameStartTime = Time.realtimeSinceStartup;
            liveDifficultyLevel = difficultyLevel;
            Monitoring.SessionManager.instance.StartNewActivity(MinigameID);
            if (gamePlayObjects != null)
            {
                gamePlayObjects.SetActive(true);
            }
        }

        public virtual void GameOver()
        {
            _playing = false;

            musicSource.Stop();
            musicSource.volume = gameOverVolume;
            if (gameOverSounds.Count > 0)
            {
                musicSource.PlayOneShot(gameOverSounds[GameManager.instance.GameRandom.Next() % gameOverSounds.Count]);
            }

            // Tells the SessionManager that the game is finished and logs should be uploaded
            Monitoring.SessionManager.instance.ActivityCompleted();
            Metrics.AchievementManager.instance.UpdateProgressData("Score", score);

            // Opens the game menu after 0.5 seconds to not make it pop up to quickly
            StartCoroutine(OpenGameMenuAfterSeconds(0.5f));
            if (gamePlayObjects != null)
            {
                gamePlayObjects.SetActive(false);
            }
        }

        public virtual IEnumerator OpenGameMenuAfterSeconds(float seconds)
        {
            yield return new WaitForSecondsRealtime(seconds);

            gameMenu.gameObject.SetActive(true);
        }

        // VerifyLoggers will be implemented in each minigamemanager to verify that each metric logger has successfully loaded in the game scene
        protected abstract void VerifyLoggers();
    }
}