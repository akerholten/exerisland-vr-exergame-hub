﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

namespace HWHub.ReactionTimeTrainer
{
    public class ReactionTrainerButton : MonoBehaviour
    {
        // Other objects in Scene
        [SerializeField] private ReactionTrainerManager manager;

        // Personal Objects

        private ForceFieldController forceFieldController;
        private AudioSource audioSource;

        [SerializeField] private Gradient activeGradient;
        [SerializeField] private Gradient nonActiveGradient;
        [SerializeField] private Gradient errorGradient;

        public bool IsActive => active;
        private bool active;

        public float ActivatedTime => _activatedTime;
        private float _activatedTime;

        private bool collisionDisabled = true;

        public int currentActivatedID;

        public enum Events
        {
            ACTIVE_BUTTON_HIT = 0,
            NONACTIVE_BUTTON_HIT = 1,
        }

        private void Start()
        {
            if (manager == null)
            {
                Debug.LogWarning("Manager reference was not preset on trainer button in scene");
                manager = FindObjectOfType<ReactionTrainerManager>();
            }

            audioSource = GetComponent<AudioSource>();
            forceFieldController = GetComponent<ForceFieldController>();
            this.InitMaterials();
            this.Deactivate();
        }

        private void OnTriggerEnter(Collider other)
        {
            if (collisionDisabled) return;

            // Left and right SteamVR hand has finger colliders
            if (other.gameObject.name.Contains("finger"))
            {
                // Display particles displaying the collision
                forceFieldController.controlParticleSystem.transform.position = other.transform.position;
                forceFieldController.controlParticleSystem.Emit(1);

                // Play impact sound
                PlayRandomSoundFromList(manager.impactClips, manager.impactClipVolume);

                // Handle haptic feedback to player
                if (other.attachedRigidbody.transform.name.Contains("HandColliderRight"))
                {
                    GameManager.instance.Haptic.Execute(0, 0.1f, 30.0f, 1.0f, SteamVR_Input_Sources.RightHand);
                }
                if (other.attachedRigidbody.transform.name.Contains("HandColliderLeft"))
                {
                    GameManager.instance.Haptic.Execute(0, 0.1f, 30.0f, 1.0f, SteamVR_Input_Sources.LeftHand);
                }

                // Handle the hit event
                this.HandleHit();
            }
        }

        private void HandleHit()
        {
            if (this.active)
            {
                // Play success sound
                PlayRandomSoundFromList(manager.successClips, manager.successClipVolume);

                this.Deactivate();

                manager.HandleButtonHit(this, Events.ACTIVE_BUTTON_HIT);
            }
            else if (!this.active)
            {
                HandleButtonError();

                manager.HandleButtonHit(this, Events.NONACTIVE_BUTTON_HIT);
            }
        }

        public void HandleButtonError()
        {
            // Play error sound
            PlayRandomSoundFromList(manager.errorClips, manager.errorClipVolume);

            // Display as error color
            forceFieldController.procedrualGradientRamp = errorGradient;
            // Queue up to go back to normal color after certain time in float seconds
            StartCoroutine(ReturnToNormalColor(0.1f));
        }

        private IEnumerator ReturnToNormalColor(float secondsToWait)
        {
            yield return new WaitForSeconds(secondsToWait);

            if (!this.active)
            {
                forceFieldController.procedrualGradientRamp = nonActiveGradient;
            }
        }

        public void Activate(int id = 0)
        {
            this.active = true;

            _activatedTime = Time.realtimeSinceStartup;

            currentActivatedID = id;

            EnableCollision();
            // Update forcefield to active colors
            forceFieldController.procedrualGradientRamp = activeGradient;

            // Play enabled sound
            PlayRandomSoundFromList(manager.enabledClips, manager.enabledClipVolume);
        }

        public void Deactivate()
        {
            this.active = false;

            // Update forcefield to nonActive colors
            forceFieldController.procedrualGradientRamp = nonActiveGradient;
        }

        public void DisableCollision()
        {
            this.collisionDisabled = true;
        }

        public void EnableCollision()
        {
            this.collisionDisabled = false;
        }

        private void PlayRandomSoundFromList(List<AudioClip> clips, float volume)
        {
            int soundToPlayId = GameManager.instance.GameRandom.Next() % clips.Count;

            PlaySoundOnce(clips[soundToPlayId], volume);
        }

        private void PlaySoundOnce(AudioClip clip, float volume)
        {
            if (audioSource != null)
            {
                audioSource.PlayOneShot(clip, volume);
            }
        }

        private void InitMaterials()
        {
            if (this.nonActiveGradient == null || this.activeGradient == null)
            {
                Debug.LogWarning("WARNING: Button material not set");
                return;
            }
        }
    }
}