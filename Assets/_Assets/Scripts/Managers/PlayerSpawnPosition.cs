﻿using HWHub.Player;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace HWHub
{
    [System.Obsolete("Obsolete for now, but kept here in case it was needed because of VR bugs anyway")]
    public class PlayerSpawnPosition : MonoBehaviour
    {
        private void Awake()
        {
            // Set correct player positions
            HWPlayer.instance.gameObject.transform.position = transform.position;
            HWPlayer.instance.gameObject.transform.rotation = transform.rotation;
        }
    }
}
