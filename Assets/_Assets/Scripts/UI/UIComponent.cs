﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace HWHub.UI
{
    public abstract class UIComponent<T> : MonoBehaviour
    {
        public T value;
        public abstract void BuildUI();
    }
}
