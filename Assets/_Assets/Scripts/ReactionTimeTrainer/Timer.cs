﻿using System;
using System.Collections;

//using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace HWHub.ReactionTimeTrainer
{
    public class Timer : MonoBehaviour
    {
        private AudioSource audioSource;
        private ReactionTrainerManager rttManager;

        [SerializeField] private Text timerText;
        [SerializeField] private uint startValue;
        [SerializeField] private float timeStep = 1.0f;

        private uint currentValue;

        public enum Events
        {
            TIMER_FINISH = 0,
            TIMER_START = 1,
        }

        private void Start()
        {
            if (audioSource == null)
            {
                audioSource = GetComponent<AudioSource>();
            }

            if (rttManager == null)
            {
                rttManager = FindObjectOfType<ReactionTrainerManager>();
            }

            if (this.timerText == null)
            {
                Debug.LogError("Error: timer holds no text object");
                return;
            }

            // Use 3 as default value for the timer
            if (this.startValue == 0)
            {
                this.startValue = 3;
            }

            this.ResetTimer();
        }

        public IEnumerator CountDown()
        {
            this.ResetTimer();

            if (rttManager == null)
            {
                rttManager = FindObjectOfType<ReactionTrainerManager>();
            }
            rttManager.HandleTimer(Events.TIMER_START);

            while (this.currentValue > 0)
            {
                this.playSound();
                yield return new WaitForSeconds(this.timeStep);
                this.currentValue -= 1;
                this.timerText.text = this.currentValue.ToString();
            }

            this.playSound(2.0f);

            if (rttManager == null)
            {
                rttManager = FindObjectOfType<ReactionTrainerManager>();
            }
            rttManager.HandleTimer(Events.TIMER_FINISH);
        }

        public void ResetTimer()
        {
            this.timerText.text = this.startValue.ToString();
            this.currentValue = this.startValue;
        }

        public void SetTimerText(string msg)
        {
            this.timerText.text = msg;
        }

        private void playSound(float pitch = 1.0f)
        {
            if (audioSource == null)
            {
                audioSource = GetComponent<AudioSource>();
            }
            audioSource.pitch = pitch;
            audioSource.Play();
        }
    }
}