﻿namespace HWHub
{
    public static class Constants
    {
        public struct Scenes
        {
            public const string MAIN_HUB = "MainHub";
            public const string PLATFORM_GAME = "PlatformMinigame";
            public const string REACTION_TIME_TRAINER = "ReactionTimeTrainer";
        }

        public const string BACKEND_URL = "https://rehab-monitoring.herokuapp.com";

        public struct Paths
        {
            public const string UploadSession = "/uploadSession";
            public const string UpdateExistingSession = "/updateExistingSession";
            public const string GenerateNewUser = "/generateNewUser";
        }
    }
}